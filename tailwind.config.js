/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        'FiraSansBold': ["FiraSansBold"],
      },
      colors: {
        'green': {
          'dav': '#50AE2F',
        },
        'blue': {
          'worms': '#033d62',
        }
      },
    },
  },
  plugins: [],
  corePlugins: {
    preflight: false,
  }
}

