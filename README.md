# React Frontend Server

## Setup the Project

1. Install npm packages with `npm install`
2. Initialize shared-submodule with `git submodule init` and `git submodule update`
3. Copy .env.dist and rename to .env
4. Start the Application with `npm run dev`

## Routing 

- You can find all defined routes in `src/App.tsx`.
- `/dashboard/*` can only be accessed by staff members and admins.
- `/` can be accessed be all users.

## Authentication and Permissions

- Verification with JWT -> stored in LocalStorage
