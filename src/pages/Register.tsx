import {
  ArrowLeftOutlined,
  EnvironmentOutlined,
  GlobalOutlined,
  HomeOutlined,
  IdcardOutlined,
  LockOutlined,
  MailOutlined,
  NumberOutlined,
  PhoneOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Button, Card, Divider, Form, Input, message } from "antd";
import { useTranslation } from "react-i18next";
import { Link, useNavigate } from "react-router-dom";
import { IUser } from "../../shared/interfaces/IUser";
import axios from "axios";
import { userAPI } from "../api/UserAPIs";

const Register = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const [messageAPI, contextHolder] = message.useMessage();

  const handleRegister = async (accountData: IUser) => {
    try {
      await userAPI.createUser(accountData);
      messageAPI.success(t("register.registration_success"), 6);
      navigate("/login");
    } catch (error) {
      if (axios.isAxiosError(error)) {
        messageAPI.error(error.message);
        messageAPI.error(error.response?.data?.error);
        console.error(error.message);
      } else {
        console.error(error);
      }
    }
  };

  return (
    <div className="py-10 m-auto">
      {contextHolder}
      <Card className="p-3 shadow-md">
        <Link to="/login">
          <Button size="large" icon={<ArrowLeftOutlined />}>
            {t("register.back_to_login")}
          </Button>
        </Link>
        <h1 className="text-center text-4xl font-extrabold text-neutral-900">
          {t("register.registration")}
        </h1>
        <Divider className="mb-0"></Divider>
        <Form
          size="large"
          className="mt-5 lg:grid grid-cols-2 gap-10"
          name="registration"
          onFinish={handleRegister}
        >
          <div>
            <Divider orientation="left">
              <span>{t("register.user_information")}</span>
            </Divider>
            <div className="flex gap-3">
              <Form.Item
                name="Firstname"
                rules={[{ required: true, message: t("message.must_field") }]}
              >
                <Input
                  prefix={<UserOutlined className="site-form-item-icon px-1" />}
                  placeholder={t("register.first_name")}
                />
              </Form.Item>
              <Form.Item
                name="Lastname"
                rules={[{ required: true, message: t("message.must_field") }]}
              >
                <Input
                  prefix={<UserOutlined className="site-form-item-icon px-1" />}
                  placeholder={t("register.last_name")}
                />
              </Form.Item>
            </div>
            <Form.Item
              name="UserId"
              rules={[
                {
                  required: true,
                  pattern: /289\/00\/[0-9]{6}/,
                  message: t("userForm.validation_member_id"),
                },
              ]}
            >
              <Input
                prefix={<IdcardOutlined className="site-form-item-icon px-1" />}
                placeholder={t("register.member_id")}
              />
            </Form.Item>
            <Form.Item
              name="Email"
              rules={[
                {
                  required: true,
                  pattern: /^[\w\-\.]+@([\w-]+\.)+[\w-]{2,}$/,
                  message: t("userForm.validation_email"),
                },
              ]}
            >
              <Input
                prefix={<MailOutlined className="site-form-item-icon px-1" />}
                placeholder={t("general.email")}
              />
            </Form.Item>
            <Form.Item
              name="Phone"
              rules={[{ required: true, message: t("message.must_field") }]}
            >
              <Input
                prefix={<PhoneOutlined className="site-form-item-icon px-1" />}
                placeholder={t("register.phone_number")}
              />
            </Form.Item>
            <Form.Item
              name="Password"
              rules={[
                {
                  required: true,
                  message: t("register.please_input_password"),
                },
              ]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon px-1" />}
                type="password"
                placeholder={t("general.password")}
              />
            </Form.Item>
          </div>
          <div>
            <Divider orientation="left">
              <span>{t("register.personal_information")}</span>
            </Divider>
            <div className="flex gap-3">
              <Form.Item
                name="Street"
                rules={[{ required: true, message: t("message.must_field") }]}
              >
                <Input
                  prefix={
                    <EnvironmentOutlined className="site-form-item-icon px-1" />
                  }
                  placeholder={t("register.street")}
                />
              </Form.Item>
              <Form.Item
                name="Housenumber"
                rules={[
                  {
                    max: 4,
                    required: true,
                    message: t("userForm.validation_housenumber"),
                  },
                ]}
              >
                <Input
                  prefix={
                    <NumberOutlined className="site-form-item-icon px-1" />
                  }
                  placeholder={t("register.house_number")}
                />
              </Form.Item>
            </div>
            <Form.Item
              name="Postal"
              rules={[
                {
                  min: 5,
                  max: 5,
                  required: true,
                  message: t("userForm.validation_postal"),
                },
              ]}
            >
              <Input
                prefix={<NumberOutlined className="site-form-item-icon px-1" />}
                placeholder={t("register.postal")}
              />
            </Form.Item>
            <Form.Item
              name="City"
              rules={[{ required: true, message: t("message.must_field") }]}
            >
              <Input
                prefix={<HomeOutlined className="site-form-item-icon px-1" />}
                placeholder={t("register.city")}
              />
            </Form.Item>
            <Form.Item
              name="Country"
              rules={[{ required: true, message: t("message.must_field") }]}
            >
              <Input
                prefix={<GlobalOutlined className="site-form-item-icon px-1" />}
                placeholder={t("register.country")}
              />
            </Form.Item>
          </div>
          <Form.Item className="col-span-2 text-center">
            <Button type="primary" block htmlType="submit" className="">
              {t("general.register")}
            </Button>
          </Form.Item>
        </Form>
        <p>
          Mit der Registrierung stimmst du unseren{" "}
          <a className="underline" href="#">
            Nutzungsbedingungen
          </a>{" "}
          und{" "}
          <a className="underline" href="#">
            Datenschutzbestimmungen
          </a>{" "}
          zu.
        </p>
      </Card>
    </div>
  );
};

export default Register;
