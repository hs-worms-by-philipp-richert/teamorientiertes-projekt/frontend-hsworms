import {
  Button,
  Modal,
  Tabs,
  TabsProps,
  message,
  Pagination,
  Tooltip,
} from "antd";
import StorageOverviewList from "../components/StorageOverviewList";
import StorageRegionOverviewList from "../components/StorageRegionOverviewList";
import { useEffect, useState } from "react";
import { PlusOutlined, InfoCircleOutlined } from "@ant-design/icons";
import StorageModal from "../components/modal/StorageModal";
import { IWarehouse } from "../../shared/interfaces/IWarehouse";
import { StorageForm } from "../interfaces/storage-form.interface";
import { useTranslation } from "react-i18next";
import { warehouseAPI } from "../api/WarehouseAPI";
import axios from "axios";
import { IWarehouseRegion } from "../../shared/interfaces/IWarehouseRegion";
import WarehouseRegionModal from "../components/modal/WarehouseRegionModal";

const StorageOverview: React.FC = () => {
  const { t } = useTranslation();

  const [showstoragemodal, setShowStorageModal] = useState<boolean>(false);
  const [warehouselist, setwarehouselist] = useState<IWarehouse[]>();
  const [loading, setLoading] = useState<boolean>(false);
  const [showRegionModal, setshowRegionModal] = useState<boolean>(false);
  const [activeTab, setActiveTab] = useState<"1" | "2">("1");
  const [selected, setSelected] = useState<IWarehouse | undefined>(undefined);
  const [messageAPI, contextHolder] = message.useMessage();

  const [totallength, settotallength] = useState<number>(0);
  const [page, setpage] = useState<number>(1);
  const defaultPageSize = 30;

  const openModal = (): void => {
    setShowStorageModal(true);
  };

  const getWarehouseList = async (
    page: number = 1,
    take: number = defaultPageSize
  ) => {
    try {
      const warehouses = await warehouseAPI.fetchWarehouses(page, take);
      setwarehouselist(warehouses.data.list);
      settotallength(warehouses.data.totalLength);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getWarehouseList();
  }, []);

  const handleWarehouseDelete = async (warehouseid: bigint) => {
    try {
      await warehouseAPI.deleteWarehouse(warehouseid);
      await getWarehouseList();
    } catch (error) {
      console.error(error);
    }
  };

  const handleWarehouseSubmit = (formdata: StorageForm) => {
    const submitWarehouse = async () => {
      try {
        setLoading(true);
        await warehouseAPI.createWarehouse(formdata);
        setShowStorageModal(false);
        await getWarehouseList();
      } catch (error) {
        if (axios.isAxiosError(error)) {
          messageAPI.error(error.message);
          console.error(error.message);
        } else {
          console.error(error);
        }
      }
      setLoading(false);
    };
    submitWarehouse();
  };

  const handleWarehouseUpdate = async (formdata: StorageForm) => {
    try {
      await warehouseAPI.updateWarehouse(
        selected?.WarehouseId as bigint,
        formdata
      );
      messageAPI.success(t("success.edit"));
      setShowStorageModal(false);
      setSelected(undefined);
      getWarehouseList();
    } catch (error) {
      console.error(error);
    }
  };

  const handleSelection = async (warehouseid: bigint) => {
    const response = await warehouseAPI.fetchWarehouse(warehouseid);
    setSelected(response.data);
    openModal();
  };

  const handleRegionSubmit = async (regiondata: IWarehouseRegion) => {
    try {
      setLoading(false);
      await warehouseAPI.createRegion(regiondata);
      messageAPI.success(t("storage_region_overview_list.success.region"));
      setLoading(true);
      setshowRegionModal(false);
    } catch (error) {
      console.error(error);
    }
  };

  const tabitems: TabsProps["items"] = [
    {
      key: "1",
      label: t("storage_overview.locations"),
      children: (
        <>
          <StorageOverviewList
            handleDelete={handleWarehouseDelete}
            handleEdit={handleSelection}
            warehouselist={warehouselist}
          ></StorageOverviewList>
          {!loading && warehouselist && warehouselist.length > 0 && (
            <div className="flex justify-center mt-8">
              <Pagination
                current={page}
                onChange={(page, pageSize) => {
                  getWarehouseList(page, pageSize);
                  setpage(page);
                }}
                total={totallength}
                pageSize={defaultPageSize}
              />
            </div>
          )}
        </>
      ),
    },
    {
      key: "2",
      label: t("storage_overview.storage_spaces"),
      children: (
        <StorageRegionOverviewList
          loading={loading}
        ></StorageRegionOverviewList>
      ),
    },
  ];

  return (
    <>
      {contextHolder}
      <Modal
        centered
        destroyOnClose
        okText={t("general.add")}
        open={showstoragemodal}
        cancelText={t("general.cancel")}
        confirmLoading={loading}
        width={"80%"}
        onCancel={() => {
          setShowStorageModal(false), setSelected(undefined);
        }}
        okButtonProps={{
          type: "primary",
          form: "warehouseForm",
          htmlType: "submit",
          icon: <PlusOutlined />,
        }}
      >
        <StorageModal
          handlesubmit={
            selected ? handleWarehouseUpdate : handleWarehouseSubmit
          }
          selected={selected}
        ></StorageModal>
      </Modal>
      <Modal
        open={showRegionModal}
        onCancel={() => setshowRegionModal(false)}
        okButtonProps={{
          type: "primary",
          form: "regionForm",
          htmlType: "submit",
          icon: <PlusOutlined />,
        }}
      >
        <WarehouseRegionModal
          onSubmit={handleRegionSubmit}
        ></WarehouseRegionModal>
      </Modal>
      <div className="flex justify-center items-center gap-2">
        <h1 className="text-gray-800 ml-auto">
          {t("general.warehouse_management")}
        </h1>
        <Tooltip
          title={t("storage_overview_list.permission_info")}
          placement="left"
        >
          <Button
            className="ml-auto"
            type="link"
            shape="circle"
            icon={<InfoCircleOutlined />}
          ></Button>
        </Tooltip>
      </div>
      <Tabs
        size="large"
        onChange={(e) => setActiveTab(e as "1" | "2")}
        items={tabitems}
        tabBarExtraContent={
          <Button
            size="large"
            onClick={
              activeTab === "1" ? openModal : () => setshowRegionModal(true)
            }
          >
            <PlusOutlined />
            <div className="hidden ml-2 md:inline">
              {activeTab === "1"
                ? t("storage_region_overview_list.add_location")
                : t("storage_region_overview_list.add_region")}
            </div>
          </Button>
        }
      ></Tabs>
    </>
  );
};

export default StorageOverview;
