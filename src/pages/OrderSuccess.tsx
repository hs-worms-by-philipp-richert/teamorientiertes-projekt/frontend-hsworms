import { Button, Result } from "antd";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Link, useLocation, useNavigate } from "react-router-dom";

const OrderSuccess = () => {
  const { t } = useTranslation();
  const location = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    if (!location.state?.ordernumber) {
      navigate("/");
    }
  });

  return (
    <Result
      title={t("order_success.success_message")}
      status="success"
      subTitle={
        <>
          <div>
            {t("order_success.order_number")} {location.state?.ordernumber}
          </div>
          <div>{t("order_success.order_message")}</div>
        </>
      }
      extra={[
        <Link to="/dashboard/verlauf">
          <Button type="primary">
            {t("order_success.navigation_order_overview")}
          </Button>
        </Link>,
      ]}
    ></Result>
  );
};

export default OrderSuccess;
