import {
  Button,
  Divider,
  Empty,
  List,
  Modal,
  Pagination,
  Popconfirm,
  Skeleton,
  Tabs,
  TabsProps,
  Tag,
  message,
} from "antd";
import { useEffect, useState } from "react";
import { IUser } from "../../shared/interfaces/IUser";
import { useTranslation } from "react-i18next";
import { userAPI } from "../api/UserAPIs";
import AdminAddUser from "../components/modal/UserFormModal";
import {
  DeleteOutlined,
  PlusOutlined,
  EditOutlined,
  MailOutlined,
  CheckOutlined,
} from "@ant-design/icons";
import Search from "antd/es/input/Search";
import axios from "axios";

const UserList = () => {
  const { t } = useTranslation();
  const [messageAPI, contextHolder] = message.useMessage();
  const [users, setusers] = useState<IUser[]>([]);
  const [toverify, setToVerify] = useState<IUser[]>();
  const [loading, setloading]: [boolean, Function] = useState(false);
  const [err, setError] = useState(false);
  const [materialModalOpen, setMaterialModalOpen]: [boolean, Function] =
    useState(false);
  const [selectedUser, setSelectedUser] = useState<IUser | undefined>(
    undefined
  );
  const [filterparams, setfilterparams] = useState({
    search: "",
    sorting: "ASC",
  });

  const [totallength, settotallength] = useState<number>(0);
  const [page, setpage] = useState<number>(1);
  const [verifypage, setverifypage] = useState<number>(1);
  const defaultPageSize = 30;

  const EditUserModal = async (user: IUser) => {
    if (user) {
      setSelectedUser(user);
      setMaterialModalOpen(true);
    }
  };

  const showAddUser = () => {
    setSelectedUser(undefined);
    setMaterialModalOpen(true);
  };

  const fetchUsers = async (
    page: number = 1,
    take: number = defaultPageSize
  ) => {
    try {
      setloading(true);
      const users = await userAPI.fetchAllUsers(page, take);
      const toverify = await userAPI.fetchAllUsers(
        page,
        take,
        { search: "", sorting: "ASC" },
        true
      );
      settotallength(users.data.totalLength);
      setusers(users.data.list);
      setToVerify(toverify.data.list);
      setloading(false);
    } catch (error) {
      setloading(false);
      setError(true);
      console.error(error || t("error.error_get_users"));
    }
  };

  useEffect(() => {
    fetchUsers();
  }, []);

  const handleFiltering = (
    searchterm: string,
    page: number = 1,
    take: number = defaultPageSize
  ) => {
    setfilterparams({ search: searchterm, sorting: "ASC" });
    const fetchfiltered = async () => {
      try {
        setloading(true);
        const users = await userAPI.fetchAllUsers(page, take, {
          search: searchterm,
          sorting: "ASC",
        });
        settotallength(users.data.totalLength);
        setloading(false);
        setusers(users.data.list);
      } catch (error) {
        setloading(false);
        setError(true);
        console.error(error || t("error.error_get_users"));
      }
    };
    fetchfiltered();
  };

  const handleEdit = async (
    formdata: IUser,
    userid: string | undefined = selectedUser?.UserId
  ) => {
    try {
      await userAPI.putUserById(userid?.replace(/\//g, "-") as string, {
        ...formdata,
        UserId: undefined,
      });
      messageAPI.success(t("success.user_edit"));
      setSelectedUser(undefined);
      setMaterialModalOpen(false);
      await fetchUsers();
    } catch (error) {
      if (axios.isAxiosError(error)) {
        if (error.response?.status == 400) {
          messageAPI.error(error.response?.data.error);
        }
      } else {
        console.error(t("error.error_add_user"), error);
      }
    }
  };

  const handleDelete = async (userId: string | undefined) => {
    if (userId) {
      try {
        await userAPI.deleteUser(userId);
        await fetchUsers();
      } catch (error) {
        setError(true);
        console.error(t("error.error_delete_user"), error);
      }
    }
  };

  const handleSubmit = async (formData: IUser) => {
    try {
      await userAPI.registerUser({
        ...formData,
        // RoleId: undefined,
      });
      messageAPI.success(t("success.user_add"));
    } catch (error) {
      if (axios.isAxiosError(error)) {
        if (error.response?.status == 400) {
          messageAPI.error(error.response?.data.error);
        }
      } else {
        console.error(t("error.error_add_user"), error);
      }
    }
  };

  const handleVerify = async (user: string) => {
    try {
      await userAPI.updateUsertoInternal(user.replace(/\//g, "-"));
      fetchUsers();
    } catch (error) {
      console.error(error);
      messageAPI.error("Bei der Verifizierung ist ein Fehler aufgetreten.");
    }
  };

  const tabitems: TabsProps["items"] = [
    {
      key: "1",
      label: t("admin.user_page_title"),
      children: (
        <>
          <div className="flex flex-row flex-wrap gap-4 w-full lg:w-1/2">
            <Search
              className="md:flex-1"
              placeholder="Suche"
              allowClear
              size="large"
              width={"100%"}
              onChange={(event) => handleFiltering(event.target.value)}
              value={filterparams.search}
            />
          </div>
          {!loading ? (
            <List
              header={
                <div className="hidden w-full md:grid md:grid-cols-3 gap-x-3 lg:grid-cols-5 items-center bg-neutral-50 font-bold text-center lg:text-start">
                  <h4 className="col-span-1 md:col-span-1 justify-self-center lg:col-span-1">
                    {t("userForm.member_nr")}
                  </h4>
                  <h4>{t("general.name")}</h4>
                  <h4 className="hidden lg:block">{t("userForm.mail")}</h4>
                  <h4 className="hidden lg:block">{t("userForm.city")}</h4>
                  <h4 className="text-center">{t("admin.action")}</h4>
                  <Divider className="m-0 col-span-5"></Divider>
                </div>
              }
              dataSource={users}
              renderItem={(item) => (
                <List.Item>
                  <div className="w-full lg:text-start text-center gap-3 grid grid-cols-1 md:grid-cols-3 lg:grid-cols-5 items-center hover:bg-gray-100">
                    <p className="col-span-1 md:col-span-1 justify-self-center font-bold w-full text-center">
                      <Tag color="purple">
                        {item.UserId?.replace(/-/g, "/")}
                      </Tag>
                    </p>
                    <p className="col-span-1 m-1 md:col-span-1 md:block font-semibold text-gray-800">
                      {`${item.Firstname} ${item.Lastname}`}
                    </p>
                    <p className="col-span-1 flex md:col-span-1 justify-center gap-1 block md:hidden lg:block font-regular text-blue-600 truncate">
                      <MailOutlined />
                      {" " + item.Email}
                    </p>
                    <p className="mt-1 md:m-0 md:col-span-1 block md:hidden lg:block">
                      {item.City}
                    </p>
                    <div className="flex md:gap-3 justify-center flex-wrap">
                      <Button
                        className="flex-1 text-blue-500"
                        icon={<EditOutlined />}
                        onClick={() => EditUserModal(item)}
                      >
                        {t("buttons.edit")}
                      </Button>
                      <Popconfirm
                        title={t("userForm.confirm_button")}
                        okText={t("general.delete")}
                        okButtonProps={{ danger: true }}
                        onConfirm={(_e) => handleDelete(item.UserId)}
                      >
                        <Button
                          danger
                          className="flex-1"
                          type="text"
                          icon={<DeleteOutlined />}
                        >
                          {t("buttons.delete")}
                        </Button>
                      </Popconfirm>
                    </div>
                  </div>
                </List.Item>
              )}
            ></List>
          ) : !err ? (
            <Skeleton paragraph={{ rows: 7 }}></Skeleton>
          ) : (
            <Empty></Empty>
          )}
          {!loading && users && users.length > 0 && (
            <div className="flex justify-center mt-8">
              <Pagination
                onChange={(page, pageSize) => {
                  handleFiltering(filterparams.search, page, pageSize);
                  setpage(page);
                }}
                current={page}
                total={totallength}
                pageSize={defaultPageSize}
              />
            </div>
          )}
        </>
      ),
    },
    {
      key: "2",
      label: t("admin.user_verify"),
      children: (
        <>
          <List
            header={
              <div className="hidden w-full md:grid md:grid-cols-3 gap-x-3 lg:grid-cols-5 items-center bg-neutral-50 font-bold text-center lg:text-start">
                <h4 className="col-span-1 md:col-span-1 justify-self-center lg:col-span-1">
                  {t("userForm.member_nr")}
                </h4>
                <h4>{t("general.name")}</h4>
                <h4 className="hidden lg:block">{t("userForm.mail")}</h4>
                <h4 className="hidden lg:block">{t("userForm.phone")}</h4>
                <h4 className="text-center">{t("admin.action")}</h4>
                <Divider className="m-0 col-span-5"></Divider>
              </div>
            }
            dataSource={toverify}
            renderItem={(item) => (
              <List.Item>
                <div className="w-full lg:text-start text-center gap-3 grid grid-cols-1 md:grid-cols-3 lg:grid-cols-5 items-center hover:bg-neutral-50">
                  <p className="col-span-1 md:col-span-1 justify-self-center font-bold w-full text-center">
                    <Tag color="purple">{item.UserId?.replace(/-/g, "/")}</Tag>
                  </p>
                  <p className="col-span-1 m-1 md:col-span-1 md:block font-semibold text-gray-800">
                    {`${item.Firstname} ${item.Lastname}`}
                  </p>
                  <p className="col-span-1 flex md:col-span-1 block md:hidden gap-1 lg:block font-regular justify-center text-blue-600 truncate">
                    <MailOutlined />
                    {" " + item.Email}
                  </p>
                  <p className="mt-1 md:m-0 md:col-span-1 block md:hidden lg:block">
                    {item.Phone}
                  </p>
                  <div className="flex md:gap-3 justify-center flex-wrap">
                    <Popconfirm
                      title={t("admin.confirm_verify")}
                      okText={t("admin.verify")}
                      okButtonProps={{ type: "primary" }}
                      onConfirm={() => handleVerify(item.UserId as string)}
                    >
                      <Button className="flex-1" icon={<CheckOutlined />}>
                        {t("admin.verify")}
                      </Button>
                    </Popconfirm>
                  </div>
                </div>
              </List.Item>
            )}
          ></List>
          {!loading && toverify && toverify.length > 0 && (
            <div className="flex justify-center mt-8">
              <Pagination
                onChange={(page, pageSize) => {
                  handleFiltering(filterparams.search, page, pageSize);
                  setverifypage(page);
                }}
                current={verifypage}
                total={totallength}
                pageSize={defaultPageSize}
              />
            </div>
          )}
        </>
      ),
    },
  ];

  return (
    <>
      {contextHolder}
      <h1 className="text-gray-800 text-center">
        {t("admin.user_page_title")}
      </h1>
      <Tabs
        size="large"
        items={tabitems}
        tabBarExtraContent={
          <Button size="large" onClick={showAddUser}>
            <PlusOutlined />
            <div className="hidden md:inline ml-2">
              {t("userForm.title_add")}
            </div>
          </Button>
        }
      ></Tabs>
      <Modal
        centered
        okText={selectedUser ? t("buttons.edit") : t("buttons.add")}
        open={materialModalOpen}
        onCancel={() => {
          setMaterialModalOpen(false);
          setSelectedUser(undefined);
        }}
        destroyOnClose
        width={"80%"}
        okButtonProps={{
          type: "primary",
          form: "userForm",
          htmlType: "submit",
          icon: <PlusOutlined />,
        }}
      >
        <AdminAddUser
          handleSubmit={handleSubmit}
          handleEdit={handleEdit}
          selecteduser={selectedUser}
        />
      </Modal>
    </>
  );
};

export default UserList;
