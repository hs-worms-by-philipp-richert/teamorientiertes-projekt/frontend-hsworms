import {
  NavigateFunction,
  Outlet,
  useLocation,
  useNavigate,
} from "react-router-dom";
import { useContext, useEffect } from "react";
import HeaderContent from "../components/HeaderContent.tsx";
import { Layout, Menu, MenuProps, Card, Button } from "antd";
import {
  SettingOutlined,
  TeamOutlined,
  HomeOutlined,
  BookOutlined,
  ProductOutlined,
  HistoryOutlined,
  SecurityScanOutlined,
  ShopOutlined,
} from "@ant-design/icons";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import Breadcrumbs from "../components/Breadcrumbs.tsx";
import { AuthContext } from "../store/auth-context.tsx";
import PermissionHelper from "../../shared/helper/permission.helper.ts";
import { Permission } from "../../shared/enums/permissions.enum.ts";

const { Header, Sider, Content } = Layout;

const RootLayout: React.FC = () => {
  const { t } = useTranslation();
  const authCtx = useContext(AuthContext);

  const sidebarItems: MenuProps["items"] = [
    {
      key: "/",
      icon: <HomeOutlined />,
      label: t("general.home"),
    },
    {
      key: "/dashboard/verlauf",
      icon: <HistoryOutlined />,
      label: t("general.renting_history"),
    },
    PermissionHelper.isAllowedTo(
      Permission.WarehouseWorker,
      PermissionHelper.parsePermissions(authCtx.getPermission())
    )
      ? {
          key: "/dashboard/bestellungen",
          icon: <BookOutlined />,
          label: t("general.orders"),
        }
      : null,
    PermissionHelper.isAllowedTo(
      Permission.WarehouseWorker,
      PermissionHelper.parsePermissions(authCtx.getPermission())
    )
      ? {
          key: "/dashboard/PSA",
          icon: <SecurityScanOutlined />,
          label: t("general.psa"),
        }
      : null,
    PermissionHelper.isAllowedTo(
      Permission.Admin,
      PermissionHelper.parsePermissions(authCtx.getPermission())
    )
      ? {
          key: "/dashboard/user",
          icon: <TeamOutlined />,
          label: t("general.user"),
        }
      : null,
    PermissionHelper.isAllowedTo(
      Permission.WarehouseWorker,
      PermissionHelper.parsePermissions(authCtx.getPermission())
    )
      ? {
          key: "/dashboard/lagerverwaltung",
          icon: <ShopOutlined />,
          label: t("general.warehouse_management"),
        }
      : null,
    PermissionHelper.isAllowedTo(
      Permission.WarehouseWorker,
      PermissionHelper.parsePermissions(authCtx.getPermission())
    )
      ? {
          key: "/dashboard/inventar",
          icon: <ProductOutlined />,
          label: t("general.inventory"),
        }
      : null,
    {
      key: "/dashboard/einstellungen",
      icon: <SettingOutlined />,
      label: t("general.settings"),
    },
  ];

  const navigate: NavigateFunction = useNavigate();
  const [collapse, setcollapse]: [boolean, Function] = useState(false);
  const handleNavigation: MenuProps["onClick"] = (event) => {
    navigate(event.key);
  };

  const location = useLocation();
  const [currentRoute, setCurrentRoute] = useState(location.pathname);

  useEffect(() => {
    setCurrentRoute(location.pathname);
  }, [location.pathname]);

  return (
    <Layout className="min-h-lvh">
      <Header className="bg-white flex justify-start items-center gap-3 px-0 h-20 drop-shadow-md z-10 sticky top-0">
        <HeaderContent></HeaderContent>
      </Header>
      <Layout>
        {authCtx.token && (
          <Sider
            theme="light"
            collapsed={collapse}
            breakpoint="lg"
            onBreakpoint={(broken) => setcollapse(broken)}
          >
            <Menu
              className="pt-5 text-start sticky top-20"
              theme="light"
              mode="inline"
              selectedKeys={[currentRoute]}
              onClick={handleNavigation}
              items={sidebarItems}
            />
          </Sider>
        )}
        <Content>
          {currentRoute !== "/" && (
            <Card className="m-4">
              <Breadcrumbs></Breadcrumbs>
            </Card>
          )}
          <Layout className="m-4 p-5 bg-white border-2 border-gray-100 border-solid rounded-lg">
            <Outlet></Outlet>
          </Layout>
        </Content>
      </Layout>
    </Layout>
  );
};

export default RootLayout;
