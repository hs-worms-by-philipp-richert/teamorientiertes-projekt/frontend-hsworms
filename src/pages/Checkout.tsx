import {
  Button,
  Descriptions,
  DescriptionsProps,
  Divider,
  Image,
  Steps,
  Table,
  TableProps,
  DatePicker,
  Checkbox,
  message,
} from "antd";
import {
  SolutionOutlined,
  UserOutlined,
  CheckOutlined,
  FilePdfOutlined,
} from "@ant-design/icons";
import MaterialOrdnung from "../assets/MaterialOrdnung.pdf";
import { CartProduct } from "../interfaces/cart-product.interface";
import { useContext, useState } from "react";
import { CartContext } from "../store/cart-context";
import { AuthContext } from "../store/auth-context";
import { useTranslation } from "react-i18next";
import { submitOrder } from "../api/CheckoutAPI";
import { useNavigate } from "react-router-dom";
import { OrderCheckout } from "../interfaces/order-checkout.interface";
import axios from "axios";
import { fileAPI } from "../api/FileAPI";

const Checkout = () => {
  const cartCtx = useContext(CartContext);
  const authCtx = useContext(AuthContext);
  const [acknowledgement, setAcknowledgement] = useState(false);
  const [messageAPI, contextHolder] = message.useMessage();
  const { t } = useTranslation();
  const navigate = useNavigate();

  const { RangePicker } = DatePicker;

  const range = Math.ceil(
    cartCtx.timespan![1].diff(cartCtx.timespan![0]) / 86400000
  );

  const handleSubmitOrder = async () => {
    const order: OrderCheckout = {
      LineItems: [],
    };
    cartCtx.products.forEach((product) => {
      product.ProductItems?.slice(0, product.amount)?.forEach((productitem) =>
        order.LineItems.push({
          ItemId: productitem.ItemId as bigint,
          StartDate: cartCtx.timespan![0].format("YYYY-MM-DD"),
          EndDate: cartCtx.timespan![1].format("YYYY-MM-DD"),
        })
      );
    });

    try {
      const response = await submitOrder(order);
      if (response.error) {
        console.error(response);
        messageAPI.error(response.error);
      } else {
        navigate("/success", { state: { ordernumber: response } });
        cartCtx.clear();
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        messageAPI.error(error.message);
        console.error(error.message);
      } else {
        console.error(error);
      }
    }
  };

  const userinfo: DescriptionsProps["items"] = [
    {
      key: "username",
      label: t("general.name"),
      children: (
        <div>{`${authCtx.userdata.Firstname} ${authCtx.userdata.Lastname}`}</div>
      ),
    },
    {
      key: "id",
      label: t("userForm.member_placeholder"),
      children: <div>{authCtx.userdata.UserId}</div>,
    },
    {
      key: "street",
      label: t("userForm.street"),
      children: (
        <div>{`${authCtx.userdata.Street}, ${authCtx.userdata.Housenumber}`}</div>
      ),
    },
    {
      key: "city",
      label: t("userForm.city"),
      children: (
        <div>{`${authCtx.userdata.Postal} ${authCtx.userdata.City}`}</div>
      ),
    },
  ];

  const rentinginfo: DescriptionsProps["items"] = [
    {
      key: "timerange",
      label: t("checkout_overview.timerange"),
      children: (
        <RangePicker
          disabled
          variant="borderless"
          className="w-full"
          value={cartCtx.timespan}
        ></RangePicker>
      ),
    },
    {
      key: "duration",
      label: t("checkout_overview.duration"),
      children: <div>{range} Tage</div>,
    },
    {
      key: "sum",
      label: t("checkout_overview.sum"),
      children: <div>{cartCtx.sum * range}€</div>,
    },
  ];

  const columns: TableProps<CartProduct>["columns"] = [
    {
      key: "productimage",
      render: (product: CartProduct) => (
        <div className="text-center">
          <Image
            height={75}
            src={
              product.ProductImages!.length > 0
                ? fileAPI.getProductImage(
                    product.ProductId!,
                    product.ProductImages![0].ImageId
                  )
                : undefined
            }
          ></Image>
        </div>
      ),
    },
    {
      title: t("general.product"),
      key: "productname",
      render: (product) => <div>{product.Name}</div>,
    },
    {
      title: t("checkout_overview.amount"),
      dataIndex: "amount",
      key: "amount",
      render: (amount) => <div>{amount}</div>,
    },
    {
      title: t("inventory_modal.product_renting_fee_placeholder"),
      dataIndex: "RentingFee",
      key: "fee",
      render: (rentingfee) => (
        <div>
          {rentingfee} {t("inventory_modal.product_renting_fee_placeholder_2")}
        </div>
      ),
    },
    {
      title: "Gesamtpreis",
      key: "amount",
      render: (product) => <div>{product.amount * product.RentingFee} €</div>,
    },
  ];

  return (
    <>
      {contextHolder}
      <Steps
        className="p-5"
        current={1}
        items={[
          {
            title: t("general.login"),
            status: "finish",
            icon: <UserOutlined />,
          },
          {
            title: t("checkout_overview.processing"),
            status: "process",
            icon: <SolutionOutlined />,
          },
          {
            title: t("checkout_overview.finish"),
            icon: <CheckOutlined />,
          },
        ]}
      />
      <div className="xl:grid grid-cols-12 mt-3">
        <div className="col-span-8">
          <h1>{t("checkout_overview.overview")}</h1>
          <Table
            columns={columns}
            dataSource={cartCtx.products.map((product) => ({
              ...product,
              key: product.ProductId,
            }))}
            className="overflow-auto"
          ></Table>
          <div className="flex items-center justify-between">
            <h1>{t("checkout_overview.personal_info")}</h1>
            <Button size="large">
              {t("checkout_overview.edit_information")}
            </Button>
          </div>
          <Descriptions bordered column={1} items={userinfo}></Descriptions>
        </div>
        <Divider className="xl:hidden"></Divider>
        <div className="flex gap-5 col-span-4 mt-4 xl:m-0">
          <Divider
            className="hidden xl:inline h-full ml-8"
            type="vertical"
          ></Divider>
          <div className="flex flex-1 justify-start flex-col gap-5">
            <Descriptions
              column={1}
              bordered
              items={rentinginfo}
            ></Descriptions>
            <div className="flex items-center">
              <Checkbox
                checked={acknowledgement}
                onChange={(e) => setAcknowledgement(e.target.checked)}
              >
                {t("checkout_overview.acknowledgement")}
              </Checkbox>
              <a
                className="text-blue-worms"
                href={MaterialOrdnung}
                target="_blank"
              >
                <FilePdfOutlined></FilePdfOutlined> PDF
              </a>
            </div>
            <Button
              block
              type="primary"
              disabled={!acknowledgement}
              onClick={handleSubmitOrder}
            >
              {t("checkout_overview.complete_order")}
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Checkout;
