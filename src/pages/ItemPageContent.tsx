import { useState, useEffect, useContext, useRef } from "react";
import {
  Button,
  DatePicker,
  InputNumber,
  Image,
  Descriptions,
  DescriptionsProps,
  Divider,
  Skeleton,
  Empty,
  Tag,
  Carousel,
  message,
} from "antd";
import {
  ShoppingCartOutlined,
  FilePdfOutlined,
  RightOutlined,
  LeftOutlined,
  CheckCircleOutlined,
  QuestionCircleOutlined,
} from "@ant-design/icons";
import { IProduct } from "../../shared/interfaces/IProduct";
import { useLocation } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { CartContext } from "../store/cart-context";
import { FALLBACKIMG } from "../constants/fallbackimage";
import dayjs, { Dayjs } from "dayjs";
import { productAPI } from "../api/ProductAPI";
import { fileAPI } from "../api/FileAPI";
import { productItemAPI } from "../api/ProductItemAPI";
import { formatPrice } from "../IntlFormat";

const { RangePicker } = DatePicker;

const ItemPageContent = () => {
  const { t } = useTranslation();

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [productdata, setProductData] = useState<IProduct>();
  const [availableitems, setAvailableitems] = useState<any>();
  const [quantity, setQuantity] = useState(1);
  const [rentalDuration, setRentalDuration] = useState(0);

  const pathname = useLocation().pathname;
  const imageurls = productdata?.ProductImages?.map((image) =>
    fileAPI.getProductImage(image.ProductId, image.ImageId)
  );

  const cartCtx = useContext(CartContext);
  const [layout, setLayout] = useState<"vertical" | "horizontal">("horizontal");
  const timeselect = useRef<any>(null);
  const [messageAPI, contextHolder] = message.useMessage();

  const getAvailableProductItems = async (start: Dayjs, end: Dayjs) => {
    if (start === null || end === null) {
      return;
    }
    const response = await productItemAPI.fetchAvailable(
      productdata?.ProductId,
      start.format("YYYY-MM-DD"),
      end.format("YYYY-MM-DD")
    );
    return response;
  };

  useEffect(() => {
    const getProduct = async () => {
      try {
        setLoading(true);
        const product = await productAPI.fetchProduct(
          BigInt(pathname.slice(9))
        );
        setProductData(product.data);
        if (productdata?.ProductId) {
          handleDateChange(cartCtx.timespan);
        }
        setLoading(false);
      } catch (error) {
        setLoading(false);
        setError(true);
        console.error(error || t("general.get_categories_error"));
      }
    };
    getProduct();
    updatelayout();
    window.addEventListener("resize", updatelayout);
    return () => window.removeEventListener("resize", updatelayout);
  }, [productdata?.ProductId]);

  const updatelayout = () => {
    window.innerWidth < 600 ? setLayout("vertical") : setLayout("horizontal");
  };

  const handleQuantityChange = (value: any) => {
    setQuantity(value);
  };

  const handleDateChange = async (dates: any) => {
    cartCtx.setTimeSpan(dates);
    if (dates && dates.length === 2) {
      const diffInDays = Math.ceil(
        (dates[1] - dates[0]) / (1000 * 60 * 60 * 24)
      );
      const available = await getAvailableProductItems(dates[0], dates[1]);
      setAvailableitems(available);
      setRentalDuration(diffInDays);
    } else {
      setRentalDuration(0);
    }
  };

  const handleaddtocart = () => {
    // const cartproduct = cartCtx.products.find(
    //   (product) => product.ProductId === availableitems.productId
    // );

    if (productdata && quantity <= availableitems.availableItems.length) {
      cartCtx.addProduct({
        ...productdata,
        amount: quantity,
        ProductItems: availableitems.availableItems,
      });
    } else {
      messageAPI.error(t("item_page_content.not_enough_items"));
    }
  };

  const showpicker = () => {
    timeselect?.current?.focus();
  };

  const calculatedItems: DescriptionsProps["items"] = [
    {
      key: 1,
      label: t("general.manufacturer"),
      children: <div>{productdata?.Manufacturer}</div>,
      span: 2,
    },
    {
      key: 2,
      label: t("general.description"),
      children: <div>{productdata?.Description}</div>,
      span: 2,
    },
    {
      key: 3,
      label: t("general.manuals"),
      children: (
        <div>
          {productdata?.ProductManuals?.map((manual) => (
            <Tag
              key={manual.ManualId}
              icon={<FilePdfOutlined />}
              className="hover:cursor-pointer"
            >
              <a target="_blanc" href={fileAPI.getProductManual(manual)}>
                {manual.Name}
              </a>
            </Tag>
          ))}
        </div>
      ),
      span: 2,
    },
    {
      key: 4,
      label: t("general.price"),
      children: `${formatPrice(productdata?.RentingFee || 0)} ${t(
        "item_page_content.renting_fee"
      )}`,
    },
    {
      key: 5,
      label: t("item_page_content.renting_period"),
      children: (
        <div className="text-center flex border-solid border-blue-worms border-dashed rounded-none border px-4 py-1 shadow-md">
          <RangePicker
            format="DD.MM.YYYY"
            ref={timeselect}
            minDate={dayjs()}
            value={cartCtx.timespan}
            defaultValue={cartCtx.timespan ?? undefined}
            variant="borderless"
            className="w-full flex-1 px-4"
            onChange={handleDateChange}
          ></RangePicker>
        </div>
      ),
    },
    {
      key: 6,
      label: t("item_page_content.renting_duration"),
      children: `${rentalDuration} ${t("item_page_content.day_count")}`,
    },
    {
      key: 7,
      label: t("item_page_content.total_price"),
      children: productdata
        ? `${formatPrice(productdata?.RentingFee * rentalDuration * quantity)}`
        : undefined,
    },
  ];

  return (
    <>
      {contextHolder}
      {!loading && !error ? (
        <div className="lg:grid lg:grid-cols-12 gap-10 py-5">
          <div className="text-center mb-3 md:mb-0 col-span-5">
            {imageurls?.length ? (
              <Carousel
                arrows
                prevArrow={<LeftOutlined />}
                nextArrow={<RightOutlined />}
              >
                {imageurls?.map((image, index) => (
                  <Image
                    key={index}
                    className="object-scale-down"
                    height={500}
                    src={image}
                  ></Image>
                ))}
              </Carousel>
            ) : (
              <Image
                className="object-scale-down"
                height={500}
                fallback={FALLBACKIMG}
              ></Image>
            )}
          </div>
          <div className="col-span-7">
            <div className="p-2 h-auto md:max-h-full w-full md:w-auto md:flex-1 overflow-auto">
              <h1 className="w-full">{productdata?.Name}</h1>
              <Divider></Divider>
              <div>
                <Descriptions
                  column={{ xs: 1, sm: 1, md: 1, lg: 1, xl: 1, xxl: 2 }}
                  items={
                    productdata?.ProductManuals?.length
                      ? calculatedItems
                      : calculatedItems.filter((item) => item.key != 3)
                  }
                  layout={layout}
                  bordered
                ></Descriptions>
              </div>
              <div className="flex flex-col justify-between gap-5 mt-10 px-10">
                <div className="flex gap-6 items-center flex-wrap">
                  <div className="flex gap-3 items-center">
                    <label htmlFor="quantity">{t("general.amount")}</label>
                    <InputNumber
                      id="quantity"
                      min={1}
                      max={5}
                      defaultValue={1}
                      onChange={handleQuantityChange}
                    />
                  </div>
                  {cartCtx.timespan ? (
                    <Tag
                      onClick={showpicker}
                      className="hover: cursor-pointer"
                      icon={<CheckCircleOutlined />}
                      color={
                        availableitems?.availableItems?.length != 0
                          ? "green"
                          : "red"
                      }
                    >
                      {availableitems?.availableItems?.length}{" "}
                      {t("general.accessible")}
                    </Tag>
                  ) : (
                    <Tag
                      color="default"
                      className="hover:cursor-pointer"
                      icon={<QuestionCircleOutlined />}
                      onClick={showpicker}
                    >
                      <span>{t("item_page_content.availability")}: </span>
                      <small>{t("item_page_content.select_time")}</small>
                    </Tag>
                  )}
                </div>
                <Button
                  className="flex-1 mt-2"
                  type="primary"
                  disabled={!cartCtx.timespan}
                  icon=<ShoppingCartOutlined />
                  onClick={handleaddtocart}
                >
                  {t("item_page_content.go_to_shopping_cart")}
                </Button>
              </div>
            </div>
          </div>
        </div>
      ) : !error ? (
        <div>
          <Skeleton paragraph={{ rows: 7 }} active />
        </div>
      ) : (
        <Empty />
      )}
    </>
  );
};

export default ItemPageContent;
