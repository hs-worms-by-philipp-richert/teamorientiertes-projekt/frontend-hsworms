import {
  Button,
  Image,
  InputNumber,
  List,
  DatePicker,
  Tooltip,
  Result,
  Descriptions,
  message,
} from "antd";
import { useContext, useEffect, useState } from "react";
import dayjs from "dayjs";
import { CartContext } from "../store/cart-context";
import { DeleteOutlined, ShoppingCartOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import { AuthContext } from "../store/auth-context";
import { CartProduct } from "../interfaces/cart-product.interface";
import { useTranslation } from "react-i18next";
import axios from "axios";
import { productItemAPI } from "../api/ProductItemAPI";
import { fileAPI } from "../api/FileAPI";

const Cart = () => {
  const { RangePicker } = DatePicker;
  const cartCtx = useContext(CartContext);
  const authCtx = useContext(AuthContext);
  const [available, setAvailable] = useState<number[]>([]);
  const { t } = useTranslation();
  const [messageAPI, contextHolder] = message.useMessage();

  const checkavailable = async (cartproduct: CartProduct) => {
    try {
      const response = await productItemAPI.fetchAvailable(
        cartproduct.ProductId,
        cartCtx.timespan![0].format("YYYY-MM-DD"),
        cartCtx.timespan![1].format("YYYY-MM-DD")
      );
      return response;
    } catch (error) {
      if (axios.isAxiosError(error)) {
        messageAPI.error(error.message);
        console.error(error.message);
      } else {
        console.error(error);
      }
    }
  };

  useEffect(() => {
    const validatecart = async () => {
      setAvailable([]);
      cartCtx.products.forEach(async (product) => {
        const response = await checkavailable(product);
        if (response.availableItems.length >= product.amount) {
          setAvailable((list) => [...list, response.availableItems.length]);
        } else {
          setAvailable((list) => [...list, 0 - response.availableItems.length]);
        }
      });
    };
    validatecart();
  }, [cartCtx.products, cartCtx.timespan]);

  const range = cartCtx.timespan
    ? Math.ceil(cartCtx.timespan?.[1].diff(cartCtx.timespan[0]) / 86400000)
    : 0;

  return (
    <>
      {contextHolder}
      <h1 className="text-center">{t("cart.title")}</h1>
      {cartCtx.products.length > 0 ? (
        <List
          header={
            <div className="hidden w-full lg:grid grid-cols-1 md:grid-cols-2 lg:grid-cols-6 content-center items-center bg-neutral-50 p-3 text-center lg:text-start font-bold">
              <div className="col-start-2">{t("general.product")}</div>
              <div className="text-center">{t("general.price")}</div>
              <div className="text-center">
                {t("item_page_content.availability")}
              </div>
              <div className="text-center">{t("general.amount")}</div>
              <div className="text-center">{t("general.actions")}</div>
            </div>
          }
          dataSource={cartCtx.products}
          renderItem={(product, index) => (
            <List.Item key={product.ProductId}>
              <div className="w-full grid grid-cols-1 md:grid-cols-2 lg:grid-cols-6 content-center items-center hover:bg-neutral-50 px-3 text-center lg:text-start">
                <div className="text-center md:col-span-2 lg:col-span-1">
                  <Image
                    className="flex justify-center object-scale-down"
                    height={100}
                    src={
                      product.ProductImages!.length > 0
                        ? fileAPI.getProductImage(
                            product.ProductId!,
                            product.ProductImages![0].ImageId
                          )
                        : undefined
                    }
                  ></Image>
                </div>
                <div className="px-3">
                  <h5 className="m-0">{product.Manufacturer}</h5>
                  <h3 className="mt-1 md:col-span-2 lg:col-span-1">
                    {product.Name}
                  </h3>
                </div>
                <p className="font-bold text-center">
                  {product.RentingFee}
                  {t("item_page_content.renting_fee")}
                </p>
                <p
                  className={`font-medium ${
                    available[index] > 0 ? "text-green-600" : "text-red-600"
                  }`}
                >
                  {available[index] > 0 ? (
                    <>
                      {t("cart.renting_possible")} <br />{" "}
                      {t("cart.products_available")}
                    </>
                  ) : range ? (
                    <>
                      {t("cart.renting_not_possible")} <br />
                      {available[index] * -1 != 0
                        ? available[index] * -1
                        : t("general.none")}
                      &nbsp;{t("cart.available")}
                    </>
                  ) : (
                    t("product_overview_list.no_timespan")
                  )}
                </p>
                <div className="flex justify-center gap-3 items-center">
                  <p>{t("general.amount")}</p>
                  <InputNumber
                    min={1}
                    value={product.amount}
                    controls={false}
                    onChange={(value: number | null) =>
                      cartCtx.setAmount({
                        ...product,
                        amount: value ? value : 1,
                      })
                    }
                  ></InputNumber>
                </div>
                <div className="flex justify-center">
                  <Tooltip title={t("cart.remove_from_cart")}>
                    <Button
                      onClick={() => cartCtx.removeProduct(product)}
                      type="text"
                      shape="circle"
                      icon={<DeleteOutlined />}
                    ></Button>
                  </Tooltip>
                </div>
              </div>
            </List.Item>
          )}
          footer={
            <>
              <div className="lg:grid grid-cols-3 mt-2">
                <Descriptions
                  bordered
                  layout="vertical"
                  className="col-start-2 col-span-2"
                >
                  <Descriptions.Item label="Ausleihzeitraum">
                    <RangePicker
                      onChange={(event: any) => cartCtx.setTimeSpan(event)}
                      size="large"
                      className="w-full"
                      value={cartCtx.timespan}
                      format={"DD.MM.YYYY"}
                      minDate={dayjs()}
                      placeholder={["Ausleihe ab", "Ausleihe bis"]}
                    ></RangePicker>
                  </Descriptions.Item>
                  <Descriptions.Item label="Summe">
                    {cartCtx.sum * range}€
                  </Descriptions.Item>
                  <Descriptions.Item label="Bestellung abschließen">
                    <Link to={authCtx.token ? "/checkout" : "/login"}>
                      <Button
                        className="px-6"
                        block
                        disabled={
                          Number.isNaN(range) ||
                          available.find((number) => number > 0)
                            ? false
                            : true
                        }
                        type="primary"
                      >
                        {t("cart.checkout")}
                      </Button>
                    </Link>
                  </Descriptions.Item>
                </Descriptions>
              </div>
            </>
          }
        ></List>
      ) : (
        <Result
          title={t("cart.cart_empty")}
          status={"404"}
          icon={<ShoppingCartOutlined />}
          extra={
            <Link to="/">
              <Button type="primary">{t("cart.navigate_back")}</Button>
            </Link>
          }
        />
      )}
    </>
  );
};

export default Cart;
