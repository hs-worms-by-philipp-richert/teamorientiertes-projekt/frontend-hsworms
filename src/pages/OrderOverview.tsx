import { Content } from "antd/es/layout/layout";
import OrderOverviewList from "../components/OrderOverviewList";
import { useTranslation } from "react-i18next";

const OrderOverview: React.FC = () => {
  const { t } = useTranslation();

  return (
    <>
      <div className="flex justify-between md:grid grid-cols-3 items-center text-center">
        <h1 className="text-gray-800 col-start-2">{t("general.orders")}</h1>
      </div>
      <Content className="p-4">
        <OrderOverviewList></OrderOverviewList>
      </Content>
    </>
  );
};

export default OrderOverview;
