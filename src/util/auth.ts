import { message } from 'antd';
import { jwtDecode } from 'jwt-decode';
import { redirect } from 'react-router';
import PermissionHelper from '../../shared/helper/permission.helper.ts';
import { Permission } from '../../shared/enums/permissions.enum';

export const getAuthToken = () => {
  const token = localStorage.getItem('_auth');

  if (!token) {
    return;
  }

  return token;
};

export const getAuthTokenExpired = (): boolean => {
  const token = localStorage.getItem('_auth');
  const payload = token ? jwtDecode(token) : undefined;

  if (!payload) return true;

  const expiration = (payload.exp ?? 0) * 1000;

  if (expiration <= Date.now()) return true;

  return false;
};

export const getUserData = () => {
  const token = localStorage.getItem('_auth');
  const { user }: any = token ? jwtDecode(token) : { user: undefined };
  if (!token) {
    return undefined;
  }
  return user;
};

export const checkAuthLoader = (): any => {
  const token = getAuthToken();
  if (!token) {
    return redirect('/login');
  }
  return null;
};

export const checkWarehouseWorker = () => {
  const user: any = getUserData();
  if (
    !PermissionHelper.isAllowedTo(Permission.WarehouseWorker, PermissionHelper.parsePermissions(user?.Role?.Permission))
  ) {
    message.error('You have no permission to access this route.');
    return redirect('/');
  }
  return null;
};

export const checkAdminLoader = () => {
  const user: any = getUserData();

  if (!PermissionHelper.isAllowedTo(Permission.Admin, PermissionHelper.parsePermissions(user?.Role?.Permission))) {
    message.error('You have no permission to access this route.');
    return redirect('/');
  }
  return null;
};
