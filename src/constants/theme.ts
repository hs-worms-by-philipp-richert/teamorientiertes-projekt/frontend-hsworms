export const THEME = {
  token: {
    colorPrimary: '#033d62',
    colorPrimaryBg: 'rgba(0,0,0,0.05)',
    fontFamily:
      'FiraSans, FiraSansBold, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji',
  },
};
