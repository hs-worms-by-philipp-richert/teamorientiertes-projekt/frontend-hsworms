import React, { useState, useEffect } from "react";
import { getAuthToken, getUserData } from "../util/auth";
import { message } from "antd";
import { authuser } from "../api/auth";
import { jwtDecode } from "jwt-decode";
import axios from "axios";
import useTokenData from "../hooks/useTokenData";
import { useTranslation } from "react-i18next";

export const AuthContext = React.createContext<{
  token: string | undefined;
  userdata: any;
  updateToken: (token: string) => void;
  logout: () => void;
  getPermission: () => number;
  login: (identifer: string, password: string) => Promise<boolean>;
}>({
  userdata: undefined,
  token: undefined,
  updateToken: (_token: string) => {},
  logout: () => {},
  getPermission: () => {
    return 0;
  },
  login: async (_identifier: string, _password: string) => {
    return false;
  },
});

const AuthContextProvider: React.FC<any> = ({ children }) => {
  const [token, setToken] = useTokenData();
  const [userdata, setUserData] = useState<any>(getUserData() ?? undefined);
  const [messageAPI, contextHolder] = message.useMessage();
  const { t } = useTranslation();

  useEffect(() => {
    if (!getAuthToken()) {
      logoutHandler();
    }
    setToken(token);
    if (token) {
      const { user }: any = jwtDecode(token);
      setUserData(user);
    }
  }, [token]);

  const updateTokenHandler = (token: string) => {
    setToken(token);
  };

  const loginHandler = async (identifier: string, password: string) => {
    try {
      const response = await authuser({ identifier, password });
      const token = response.data.accessToken;
      const { user }: any = jwtDecode(token);
      messageAPI.success(t("login.login_message"));
      localStorage.setItem("_auth", token);
      localStorage.setItem("_auth_state", user);
      setUserData(user);
      setToken(token);
      return true;
    } catch (error) {
      if (axios.isAxiosError(error)) {
        messageAPI.error(t("login.error.message"));
        console.error(error);
      } else {
        console.error(error);
      }
      return false;
    }
  };

  const logoutHandler = () => {
    setToken(undefined);
    localStorage.removeItem("_auth");
    messageAPI.info(t("login.logout_message"));
  };

  const permissionHandler = () => {
    const permission: any = userdata?.Role?.Permission;
    return permission;
  };

  const contextValue = {
    token: token,
    userdata: userdata,
    updateToken: updateTokenHandler,
    logout: logoutHandler,
    login: loginHandler,
    getPermission: permissionHandler,
  };

  return (
    <AuthContext.Provider value={contextValue}>
      {contextHolder}
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContextProvider;
