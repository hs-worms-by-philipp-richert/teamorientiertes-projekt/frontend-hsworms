import axiosBase from './axiosBase';
import { OrderFilter } from '../interfaces/order-filter.inteface';
import { OrderStatus } from '../../shared/enums/order-status.enum';

class OrderAPI {
  private BACKENDHOST: string;

  constructor() {
    this.BACKENDHOST = import.meta.env.VITE_REACT_API_URL;
  }

  async getOrders(
    page: number,
    take: number,
    filterparams: OrderFilter = {
      searchterm: '',
      sort: 'DESC',
      orderDate: '',
      orderStatus: '',
    }
  ) {
    const orders = await axiosBase.get(
      `${this.BACKENDHOST}/order/list?page=${page}&take=${take}&search=${filterparams.searchterm}&sort=${filterparams.sort}&orderDate=${filterparams.orderDate}&statusOrder=${filterparams.orderStatus}`
    );

    return orders;
  }

  async getOrder(id: string) {
    const order = await axiosBase.get(`${this.BACKENDHOST}/order/${id}`);

    return order;
  }

  async getOrderByUser(userid: bigint, page: number, take: number) {
    const orders = await axiosBase.get(`${this.BACKENDHOST}/order/user/${userid}?page=${page}&take=${take}`);
    return orders.data;
  }

  async changeStatustoPickup(id: any) {
    await axiosBase.put(`${this.BACKENDHOST}/order/${id}/status/finalize-pickup`);
  }

  async changeStatus(id: any, status: OrderStatus) {
    await axiosBase.put(`${this.BACKENDHOST}/order/${id}/status?orderStatus=${status}`);
  }

  async postReturnDate(lineItemId: bigint) {
    await axiosBase.put(`${this.BACKENDHOST}/line-item/set-return-date/${lineItemId}`);
  }

  async setReturnDateToNull(lineItemId: bigint) {
    await axiosBase.put(`${this.BACKENDHOST}/line-item/set-return-date-to-null/${lineItemId}`);
  }
}

export const orderAPI = new OrderAPI();
