import axiosBase from './axiosBase';
import { IProduct } from '../../shared/interfaces/IProduct';
import { ProductFilters } from '../interfaces/product-filters.interface';

class ProductAPI {
  private BACKENDHOST: string;

  constructor() {
    this.BACKENDHOST = import.meta.env.VITE_REACT_API_URL;
  }

  async fetchProduct(id: bigint | undefined) {
    const product = await axiosBase.get(`${this.BACKENDHOST}/product/${id}`);
    return product;
  }

  async fetchProducts(
    page: number = 1,
    take: number = 30,
    filterparams: ProductFilters = {
      searchterm: '',
      sort: 'ASC',
      categories: '',
      startdate: '',
      enddate: '',
    }
  ) {
    const products = await axiosBase.get(
      `${this.BACKENDHOST}/product/list?page=${page}&take=${take}&search=${filterparams.searchterm}&sort=${filterparams.sort}&categories=${
        filterparams.categories
      }&startDate=${filterparams.startdate ?? ''}&endDate=${filterparams.enddate ?? ''}`
    );
    return products;
  }

  async postProduct(productData: IProduct): Promise<string> {
    try {
      const response = await axiosBase.post(`${this.BACKENDHOST}/product/`, productData, {
        headers: {
          'Content-Type': 'application/json'
        }
      });
      return response.data.ProductId;
    } catch (error) {
      console.error('Während der Erstellung des Produkts ist ein Fehler geschehen:', error);
      throw error;
    }
  }

  async postProductImage(productId: bigint, fileList: File[]) {
    try {
      fileList.forEach((file) => {
        const formData = new FormData();
        formData.append('file', file);

        axiosBase.post(`${this.BACKENDHOST}/file/upload/product-image/${productId}`, formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        });
      });
    } catch (error) {
      throw new Error('Error uploading product images');
    }
  }

  async postProductManual(productId: bigint, fileList: File[]) {
    try {
      fileList.forEach((file) => {
        const formData = new FormData();
        formData.append('file', file);

        axiosBase.post(`${this.BACKENDHOST}/file/upload/product-manual/${productId}`, formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        });
      });
    } catch (error) {
      throw new Error('Error uploading product manuals');
    }
  }

  async updateProduct(product: IProduct) {
    await axiosBase.put(`${this.BACKENDHOST}/product/${product.ProductId}`, product, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }

  async deleteProduct(id: bigint) {
    await axiosBase.delete(`${this.BACKENDHOST}/product/${id}`);
  }

  async deleteProductImage(productid: bigint, imageid: string) {
    await axiosBase.delete(`${this.BACKENDHOST}/file/product-image/${productid}/${imageid}`);
  }

  async deleteProductManual(productid: bigint, manualid: string) {
    await axiosBase.delete(`${this.BACKENDHOST}/file/product-manual/${productid}/${manualid}`);
  }
}

export const productAPI = new ProductAPI();
