class PdfAPI {
  private BACKENDHOST: string;

  constructor() {
    this.BACKENDHOST = import.meta.env.VITE_REACT_API_URL;
  }

  getPdf(orderId: bigint | undefined) {
    if (!orderId) return 'not-found';
    return `${this.BACKENDHOST}/pdf/${orderId}`;
  }
}

export const pdfAPI = new PdfAPI();
