import logo from "../assets/logo-hs-worms.png";
import { Link, useNavigate } from "react-router-dom";
import { useContext } from "react";
import { Tooltip, Button, Badge } from "antd";
import { UserOutlined, ShoppingOutlined } from "@ant-design/icons";
import { useTranslation } from "react-i18next";
import { CartContext } from "../store/cart-context";
import { AuthContext } from "../store/auth-context";

const HeaderContent: React.FC = () => {
  const cartCtx = useContext(CartContext);
  const authctx = useContext(AuthContext);
  const { t } = useTranslation();
  const navigate = useNavigate();

  const handleLogout = () => {
    localStorage.removeItem("token");
    authctx.logout();
    navigate("/");
  };

  return (
    <>
      <Link
        to="/"
        className="flex justify-center items-center ml-6 mr-4"
        style={{ width: 200 }}
      >
        <img className="object-scale-down w-28" src={logo} alt="logo" />
      </Link>
      <h1 className="sm:block max-sm:hidden text-nowrap text-blue-worms text-xl">
        {t("header.title")}
      </h1>
      <div className="flex w-full items-center justify-end gap-3">
        {authctx.token && (
          <Button onClick={handleLogout} size="large">
            Logout
          </Button>
        )}
        {!authctx.token && (
          <Link to="/login">
            <Tooltip title="User">
              <Button
                size="large"
                shape="circle"
                type="text"
                icon={<UserOutlined />}
              />
            </Tooltip>
          </Link>
        )}

        <Link to="/warenkorb">
          <Tooltip title="Warenkorb" className="mr-5">
            <Badge count={cartCtx.products.length}>
              <Button size="large" shape="circle" icon={<ShoppingOutlined />} />
            </Badge>
          </Tooltip>
        </Link>
      </div>
    </>
  );
};

export default HeaderContent;
