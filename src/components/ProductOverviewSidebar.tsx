import { Menu, MenuProps, Skeleton, Tag } from "antd";
import { useEffect, useState } from "react";
import { ICategory } from "../../shared/interfaces/ICategory";
import { useTranslation } from "react-i18next";
import { categoryAPI } from "../api/CategoryAPI";

const ProductOverviewSidebar: React.FC<{ onCategoryChange: Function }> = ({
  onCategoryChange,
}) => {
  const { t } = useTranslation();

  const [loading, setLoading] = useState(false);
  const [err, setError] = useState(false);
  const [categorydata, setCategoryData] = useState<ICategory[]>([]);
  const [openCategory, setOpenCategory] = useState<string[]>([""]);
  const [selectedCategories, setSelectedCategory] = useState<string[]>([]);

  const getCategories = async () => {
    try {
      setLoading(true);
      const categories = await categoryAPI.fetchCategories(false, true);
      setCategoryData(categories.data.list);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      setError(true);
      console.error(error || t("general.get_categories_error"));
    }
  };

  const onSelectCategory = (categoryid: string) => {
    if (!selectedCategories.includes(categoryid)) {
      setSelectedCategory([categoryid]);
      onCategoryChange(categoryid);
    }
  };

  useEffect(() => {
    getCategories();
  }, []);

  const onOpenChange: MenuProps["onOpenChange"] = (keys: string[]) => {
    const latestOpenKey = keys.find((key) => openCategory.indexOf(key) === -1);
    setOpenCategory(latestOpenKey ? [latestOpenKey] : []);
  };

  const transformdata = (categories: ICategory[]) => {
    const transformed = categories?.map((category) => {
      return {
        key: Number(category.CategoryId),
        icon: "",
        label: category.Name,
        onTitleClick: ({ key }: { key: string }) => onSelectCategory(key),
        children: category.ChildCategories?.map((childCat) => {
          return {
            key: Number(childCat.CategoryId),
            icon: "",
            label: childCat.Name,
          };
        }),
      };
    });

    return transformed?.map((category) => ({
      ...category,
      children: category.children?.length === 0 ? undefined : category.children,
    }));
  };

  const getCategoryNameById = (id: string) => {
    const searchArr: ICategory[] = [];

    for (const obj of categorydata) {
      const { ChildCategories, ...rest } = obj;
      searchArr.push(rest);

      if (ChildCategories) searchArr.push(...ChildCategories);
    }

    return searchArr.find((m) => m.CategoryId?.toString() == id)?.Name;
  };

  const getCategoryTag = (i: number) => {
    if (!selectedCategories || selectedCategories.length == 0) return "";

    const tag = (
      <Tag
        bordered={false}
        closable
        onClose={(e) => {
          e.preventDefault();
          onCategoryChange("");
          setSelectedCategory([]);
          setOpenCategory([""]);
        }}
        key={i}
      >
        {getCategoryNameById(selectedCategories[i]) ?? "-"}
      </Tag>
    );

    return tag;
  };

  return (
    <>
      <h2 className="text-gray-700 mt-5 text-center">
        {t("general.categories")}
      </h2>
      <div style={{ textAlign: "center", marginBottom: "10px" }}>
        {Array.from(selectedCategories)?.map((_m, i) => getCategoryTag(i))}
      </div>
      {!loading && !err ? (
        <Menu
          mode="inline"
          className="text-start"
          openKeys={openCategory}
          onClick={({ key }) => {
            onSelectCategory(key);
          }}
          onOpenChange={onOpenChange}
          items={transformdata(categorydata)}
        />
      ) : (
        <Skeleton active title={false} paragraph={{ rows: 8 }}></Skeleton>
      )}
    </>
  );
};

export default ProductOverviewSidebar;
