import { Button, Divider, List, Popconfirm, Skeleton } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { IProductItem } from "../../shared/interfaces/IProductItem";
import { useTranslation } from "react-i18next";

const ItemInventoryOverviewList: React.FC<{
  items: IProductItem[];
  loading: boolean;
  handleEdit: (productitem: IProductItem) => void;
  handleDelete: (productid: bigint) => void;
}> = ({ items, loading, handleEdit, handleDelete }) => {
  const { t } = useTranslation();

  return (
    <>
      {!loading ? (
        <List
          itemLayout="vertical"
          dataSource={items}
          header={
            <div className="hidden md:grid grid-cols-3 bg-neutral-50 font-bold">
              <p className="ml-5">
                {t("item_inventory_overview.add_modal.qrcode")}
              </p>
              <p>{t("item_inventory_overview.tray")}</p>
              <p>{t("general.actions")}</p>
              <Divider className="lg:col-span-3 m-0"></Divider>
            </div>
          }
          renderItem={(item, index) => (
            <List.Item key={index}>
              <div className="grid grid-cols-1 md:grid-cols-3 content-center items-center hover:bg-neutral-50 px-3 text-center md:text-start p-2">
                <p className="px-3 font-bold bg-gray-100 md:bg-transparent">
                  {item.QrCode}
                </p>
                <p className="font-semibold">
                  {item.Tray?.Name.toString() || ""}
                </p>
                <div className="flex-1 flex flex-wrap">
                  <Button
                    className="text-blue-500 flex-1"
                    icon={<EditOutlined />}
                    onClick={() => handleEdit(item)}
                  >
                    {t("general.edit")}
                  </Button>
                  <Popconfirm
                    title={t("item_inventory_overview.confirm_delete")}
                    onConfirm={() => handleDelete(item.ItemId!)}
                  >
                    <Button
                      className="flex-1"
                      danger
                      type="link"
                      icon={<DeleteOutlined />}
                    >
                      {t("general.delete")}
                    </Button>
                  </Popconfirm>
                </div>
              </div>
            </List.Item>
          )}
        ></List>
      ) : (
        <div>
          <List.Item>
            <div className="w-full">
              <Skeleton></Skeleton>
              <Skeleton></Skeleton>
            </div>
          </List.Item>
        </div>
      )}
    </>
  );
};

export default ItemInventoryOverviewList;
