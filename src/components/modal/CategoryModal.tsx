import { Form, Input, Divider, Select } from "antd";
import { useEffect, useState } from "react";
import { ICategory } from "../../../shared/interfaces/ICategory";
import { useTranslation } from "react-i18next";
import { categoryAPI } from "../../api/CategoryAPI";

const CategoryModal = ({
  handlesubmit,
  selectedCategory,
}: {
  handlesubmit: Function;
  selectedCategory: ICategory | undefined | null;
}) => {
  const { t } = useTranslation();

  const [categories, setCategories]: [ICategory[], Function] = useState([]);

  useEffect(() => {
    const getCategories = async () => {
      try {
        const categories = await categoryAPI.fetchCategories();
        setCategories(categories.data.list);
      } catch (error) {
        console.error(error || t("general.get_categories_error"));
      }
    };
    getCategories();
  }, []);

  return (
    <div className="w-full">
      <h1 className="text-center">
        {t("general.category")}{" "}
        {selectedCategory ? t("general.edit") : t("general.add")}
      </h1>
      <Divider></Divider>
      <Form
        preserve={false}
        className="p-4 mt-4"
        id="categoryForm"
        layout="vertical"
        onFinish={(fieldsvalue) =>
          handlesubmit(fieldsvalue.Name, fieldsvalue.CategoryParentId)
        }
      >
        <Form.Item
          label={t("category_modal.name_of_category")}
          name="Name"
          initialValue={selectedCategory?.Name}
        >
          <Input></Input>
        </Form.Item>
        {(selectedCategory === null ||
          !selectedCategory ||
          selectedCategory?.ParentCategoryId) && (
          <Form.Item
            label={t("category_modal.optional_parent_category")}
            name="CategoryParentId"
            initialValue={
              selectedCategory && selectedCategory.ParentCategoryId?.CategoryId
            }
          >
            <Select
              allowClear
              options={categories
                .filter((item) => item.ParentCategoryId === null)
                .map((category) => ({
                  label: category.Name,
                  value: category.CategoryId,
                }))}
            ></Select>
          </Form.Item>
        )}
      </Form>
    </div>
  );
};

export default CategoryModal;
