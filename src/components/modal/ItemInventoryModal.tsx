import React, { useEffect, useState } from "react";
import { DatePicker, Divider, Form, Input, Select, TreeSelect } from "antd";
import { useTranslation } from "react-i18next";
import { IProductItem } from "../../../shared/interfaces/IProductItem";
import fetchWarehouseRegions from "../../api/fetchWarehouseRegion";
import { IWarehouseRegion } from "../../../shared/interfaces/IWarehouseRegion";
import { IWarehouseStorage } from "../../../shared/interfaces/IWarehouseStorage";
import { IWarehouseStorageTray } from "../../../shared/interfaces/IWarehouseStorageTray";
import { DefaultOptionType } from "antd/es/select";
import dayjs from "dayjs";

const ItemInventoryModal: React.FC<{
  onSubmit: (values: any) => Promise<void>;
  selectedproductitem: IProductItem | undefined;
}> = ({ onSubmit, selectedproductitem }) => {
  const { t } = useTranslation();
  // const [regionList, setRegions] = useState<IWarehouseRegion[]>([]);
  const [treeData, setTreeData] = useState<Omit<DefaultOptionType, "label">[]>(
    []
  );

  const getRegionData = (page: number = 1, take: number = 1000) => {
    const getData = async () => {
      try {
        const trays = await fetchWarehouseRegions(page, take);
        setTree(trays.data.list);
      } catch (error) {
        console.error(error);
      }
    };
    getData();
  };

  const setTree = (regions: IWarehouseRegion[]) => {
    let data: Omit<DefaultOptionType, "label">[] = regions.map(
      (region: IWarehouseRegion) => ({
        title: region.Name || "",
        value: `region-${region.RegionId}`,
        selectable: false,
        children: region.Storages?.map((storage: IWarehouseStorage) => ({
          value: `storage-${storage.StorageId}`,
          title: storage.Name || "",
          selectable: false,
          children: storage.Trays?.map((tray: IWarehouseStorageTray) => ({
            value: tray.TrayId,
            title: tray.Name || "",
          })),
        })),
      })
    );

    setTreeData(data);
  };

  useEffect(() => {
    getRegionData();
  }, []);

  return (
    <>
      <h1>{t("item_inventory_overview.add_item")}</h1>
      <Divider className="m-0"></Divider>
      <Form
        className="p-4 mt-4"
        id="subModalForm"
        layout="vertical"
        initialValues={{
          ...selectedproductitem,
          ManufacturingDate: dayjs(selectedproductitem?.ManufacturingDate),
        }}
        onFinish={onSubmit}
      >
        <div className="md:grid grid-cols-2 gap-5">
          <Form.Item
            name="QrCode"
            label={t("item_inventory_overview.add_modal.qrcode")}
            rules={[{ required: true }]}
          >
            <Input placeholder="QrCode"></Input>
          </Form.Item>
          <Form.Item
            name="Serialnumber"
            label={t("item_inventory_overview.serialnumber")}
          >
            <Input placeholder="Seriennummer"></Input>
          </Form.Item>
          <Form.Item
            name="TrayId"
            label={t("storage_region_overview_list.tray")}
            rules={[{ required: true }]}
          >
            <TreeSelect
              showSearch
              style={{ width: "100%" }}
              dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
              placeholder="Bitte Auswählen"
              treeDefaultExpandAll
              treeData={treeData}
            />
            {/* <Select options={trays.map((tray) => {({label: tray.Name, value: tray.StorageId})})}>

            </Select> */}
          </Form.Item>
          <Form.Item
            name="ManufacturingDate"
            label={t("item_inventory_overview.add_modal.manufacturingdate")}
          >
            <DatePicker placeholder="Select a date" format={"DD.MM.YYYY"} />
          </Form.Item>
          <Form.Item
            name="IsDefect"
            label={t("item_inventory_overview.add_modal.isdefect")}
            initialValue={false}
            rules={[{ required: true }]}
          >
            <Select>
              <Select.Option value={true}>{t("general.yes")}</Select.Option>
              <Select.Option value={false}>{t("general.no")}</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="ExpiresInYears"
            label={t("item_inventory_overview.add_modal.expiresyears")}
          >
            <Input
              placeholder={t("item_inventory_overview.add_modal.expiresyears")}
              type="number"
            ></Input>
          </Form.Item>
          <Form.Item
            name="IsPsa"
            label={t("item_inventory_overview.add_modal.ispsa")}
            initialValue={false}
            rules={[{ required: true }]}
          >
            <Select>
              <Select.Option value={true}>{t("general.yes")}</Select.Option>
              <Select.Option value={false}>{t("general.no")}</Select.Option>
            </Select>
          </Form.Item>
        </div>
      </Form>
    </>
  );
};

export default ItemInventoryModal;
