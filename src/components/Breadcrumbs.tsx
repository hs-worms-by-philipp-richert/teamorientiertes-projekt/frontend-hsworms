import { useLocation, Link } from "react-router-dom";
import { Breadcrumb } from "antd";
import { useTranslation } from "react-i18next";

const BreadCrumb = () => {
  const { t } = useTranslation();
  const location = useLocation();
  const breadCrumbView = () => {
    const { pathname } = location;
    const pathnames = pathname.split("/").filter((item) => item);
    return (
      <div>
        <Breadcrumb>
          {pathnames.length > 0 ? (
            <Breadcrumb.Item>
              <Link to="/">{t("breadcrumbs.home")}</Link>
            </Breadcrumb.Item>
          ) : (
            <Breadcrumb.Item>{t("breadcrumbs.home")}</Breadcrumb.Item>
          )}
          {pathnames.map((name, index) => {
            if (containsNumber(name)) {
              return <Breadcrumb.Item key={index}>{name}</Breadcrumb.Item>;
            }
            const routeTo = createRoute(
              `/${pathnames.slice(0, index + 1).join("/")}`
            );
            const isLast = index === pathnames.length - 1;
            return isLast ? (
              <Breadcrumb.Item key={index}>
                {t(`breadcrumbs.${name.toLowerCase()}`)}
              </Breadcrumb.Item>
            ) : (
              <Breadcrumb.Item key={index}>
                <Link to={`${routeTo}`}>{t(`breadcrumbs.${name}`)}</Link>
              </Breadcrumb.Item>
            );
          })}
        </Breadcrumb>
      </div>
    );
  };

  return <>{breadCrumbView()}</>;
};

const createRoute = (str: string): string => {
  const routeMap: Record<string, string> = {
    "/produkt": "/",
    "/dashboard": "/",
    "/dashboard/bestellungen/order": "/dashboard/bestellungen",
    "/dashboard/psa/": "",
  };

  return routeMap[str] || str;
};

const containsNumber = (str: string): boolean => {
  return /\d/.test(str);
};

export default BreadCrumb;
