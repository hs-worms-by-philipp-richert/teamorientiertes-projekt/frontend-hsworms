import {
  Button,
  Divider,
  List,
  Modal,
  Pagination,
  Popconfirm,
  Select,
  Skeleton,
  Tag,
  message,
} from "antd";
import Search from "antd/es/input/Search";
import axios from "axios";
import { DeleteOutlined, EditOutlined, PlusOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";
import { IProduct } from "../../shared/interfaces/IProduct";
import InventoryModal from "./modal/InventoryModal";
import { NavigateFunction, useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { productAPI } from "../api/ProductAPI";
import { ProductFilters } from "../interfaces/product-filters.interface";

const InventoryOverviewList = ({
  modalopen,
  setmodalopen,
}: {
  modalopen: { showCategoryModal: boolean; showMaterialModal: boolean };
  setmodalopen: Function;
}) => {
  const { t } = useTranslation();

  const [messageAPI, contextHolder] = message.useMessage();

  const [products, setproducts] = useState<IProduct[]>([]);
  const [loading, setloading] = useState<boolean>(false);
  const [filterparams, setfilterparams] = useState<ProductFilters>({
    searchterm: "",
    sort: "ASC",
    categories: "",
  });
  const [selectedProduct, setSelectedProduct] = useState<
    IProduct | undefined | null
  >();

  const [totallength, settotallength] = useState<number>(0);
  const [page, setpage] = useState<number>(1);
  const defaultPageSize = 30;

  const navigate: NavigateFunction = useNavigate();

  const closeModal = () => {
    setmodalopen(false, false);
    setSelectedProduct(null);
  };

  const fetchProductList = (
    page: number = 1,
    take: number = defaultPageSize
  ) => {
    const fetchData = async () => {
      try {
        closeModal();
        setloading(true);
        const response = await productAPI.fetchProducts(
          page,
          take,
          filterparams
        );
        const products = response.data;
        setproducts([...products.listAvailable, ...products.listUnavailable]);
        settotallength(products.totalAvailable + products.totalUnavailable);
        setloading(false);
      } catch (error) {
        setloading(false);
        console.error(
          error || t("inventory_overview_list.get_categories_error")
        );
      }
    };
    fetchData();
  };

  useEffect(() => {
    fetchProductList();
  }, [filterparams.searchterm]);

  const handleFiltering = (searchterm: string) => {
    setfilterparams((filters) => ({ ...filters, searchterm: searchterm }));
  };

  const handleDelete = async (productId: bigint | undefined) => {
    if (productId) {
      try {
        await productAPI.deleteProduct(productId);
        setproducts(
          products.filter((product) => product.ProductId != productId)
        );
      } catch (error) {
        if (axios.isAxiosError(error)) {
          messageAPI.error(error.message);
          console.error(error.message);
        } else {
          console.error(error);
        }
      }
    }
  };

  const handleAvailabilityUpdate = async (
    product: IProduct,
    isInternal: boolean
  ) => {
    try {
      await productAPI.updateProduct({
        ...product,
        IsInternal: isInternal,
        ProductImages: undefined,
        ProductItems: undefined,
        ProductManuals: undefined,
        Variants: undefined,
        Categories: undefined,
      });
      messageAPI.success("Zugänglichkeit wurde aktualisiert");
      fetchProductList();
    } catch (error) {
      console.error(error);
    }
  };

  const handleProductEdit = async (product: IProduct | undefined) => {
    try {
      const response = await productAPI.fetchProduct(product?.ProductId);
      if (product) {
        setmodalopen({ showCategoryModal: false, showMaterialModal: true });
        setSelectedProduct(response.data);
      } else {
        console.error(t("inventory_overview_list.get_product_error"));
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleProductNavigation = (id: bigint): void => {
    navigate(`${id}`);
  };

  return (
    <>
      {contextHolder}
      <Search
        className="flex-1 lg:w-1/2"
        placeholder={t("general.search")}
        allowClear
        size="large"
        onChange={(event) => handleFiltering(event.target.value)}
        value={filterparams.searchterm}
      />
      {!loading ? (
        <List
          header={
            <div className="hidden w-full md:grid md:grid-cols-5 lg:grid-cols-6 items-center bg-neutral-50 font-bold">
              <p className="col-span-1 justify-self-center">ID</p>
              <p className="col-span-1">{t("general.product")}</p>
              <p className="lg:col-span-2">{t("general.categories")}</p>
              <p className="col-span-1">{t("general.accessibility")}</p>
              <p className="text-center">{t("general.actions")}</p>
              <Divider className="md:col-span-5 lg:col-span-6 m-0"></Divider>
            </div>
          }
          dataSource={products}
          renderItem={(item) => (
            <List.Item>
              <div
                onClick={() => handleProductNavigation(item.ProductId!)}
                className="w-full grid grid-cols-1 md:grid-cols-5 lg:grid-cols-6 text-center md:text-start items-center gap-5 hover:bg-neutral-50 px-3 hover:cursor-pointer"
              >
                <p className="col-span-1 md:col-span-1 justify-self-center font-[FiraSansBold] bg-gray-100 md:bg-transparent w-full text-center">
                  <span className="md:hidden mr-2">{t("general.product")}</span>
                  #{item.ProductId?.toString()}
                </p>
                <div className="col-span-1 md:col-span-1">
                  <div>{item.Manufacturer}</div>
                  <h4 className="m-0 text-gray-700">{item.Name}</h4>
                </div>
                <div className="col-span-1 lg:col-span-2">
                  <div className="flex justify-center md:justify-start">
                    {item.Categories?.map((category, index) => (
                      <Tag
                        color="purple"
                        className="px-2"
                        key={index}
                        onClick={(e) => e.stopPropagation()}
                      >
                        {category.Category?.Name}
                      </Tag>
                    ))}
                  </div>
                </div>
                <div className="col-span-1">
                  {/* <Tag className="w-1/2 text-center" color="blue">
                    {item.IsInternal ? "internal" : t("general.external")}
                  </Tag> */}
                  <Select
                    onClick={(e) => e.stopPropagation()}
                    className="w-full"
                    value={item.IsInternal}
                    onChange={(value: boolean) =>
                      handleAvailabilityUpdate(item, value)
                    }
                    options={[
                      {
                        value: true,
                        label: t("general.internal"),
                      },
                      {
                        value: false,
                        label: t("general.external"),
                      },
                    ]}
                  ></Select>
                </div>
                <div className="flex justify-center flex-wrap col-span-1 md:col-span-1 flex-row items-center gap-2">
                  <Button
                    className="flex-1 text-blue-500"
                    icon={<EditOutlined />}
                    onClick={(e) => {
                      e.stopPropagation();
                      handleProductEdit(item);
                    }}
                  >
                    {t("general.edit")}
                  </Button>
                  <div onClick={(e) => e.stopPropagation()}>
                    <Popconfirm
                      title={t(
                        "inventory_overview_list.delete_eqipment_confirmation"
                      )}
                      onConfirm={() => handleDelete(item.ProductId)}
                    >
                      <Button
                        danger
                        type="link"
                        className="flex-1"
                        icon={<DeleteOutlined />}
                        onClick={(e) => e.stopPropagation()}
                      >
                        {t("general.delete")}
                      </Button>
                    </Popconfirm>
                  </div>
                </div>
              </div>
            </List.Item>
          )}
        ></List>
      ) : (
        <div>
          <List.Item>
            <div className="w-full">
              <Skeleton></Skeleton>
              <Skeleton></Skeleton>
            </div>
          </List.Item>
        </div>
      )}

      {!loading && products && products.length > 0 && (
        <div className="flex justify-center mt-8">
          <Pagination
            onChange={(page, pageSize) => {
              fetchProductList(page, pageSize), setpage(page);
            }}
            total={totallength}
            current={page}
            pageSize={defaultPageSize}
          />
        </div>
      )}

      <Modal
        centered
        destroyOnClose
        open={modalopen.showMaterialModal}
        onCancel={closeModal}
        width={"75%"}
        okText={selectedProduct ? t("general.edit") : t("general.add")}
        cancelText={t("general.cancel")}
        okButtonProps={{
          type: "primary",
          form: "inventoryForm",
          htmlType: "submit",
          icon: <PlusOutlined />,
        }}
      >
        <InventoryModal
          selecteditem={selectedProduct}
          updateproducts={fetchProductList}
        ></InventoryModal>
      </Modal>
    </>
  );
};

export default InventoryOverviewList;
