import { NavigateFunction, useNavigate } from "react-router-dom";
import {
  Image,
  Select,
  Button,
  Divider,
  List,
  DatePicker,
  Skeleton,
  Pagination,
} from "antd";
import { IProduct } from "../../shared/interfaces/IProduct";
import { PagedAvailabilityList } from "../../shared/models/paged-list.model";
import {
  forwardRef,
  useContext,
  useEffect,
  useImperativeHandle,
  useState,
} from "react";
import dayjs, { Dayjs } from "dayjs";
import Search from "antd/es/input/Search";
import { ShoppingCartOutlined } from "@ant-design/icons";
import { RefCall } from "../interfaces/ref-call.interface";
import { useTranslation } from "react-i18next";
import { CartContext } from "../store/cart-context";
import { ProductFilters } from "../interfaces/product-filters.interface";
import { productAPI } from "../api/ProductAPI";
import { FALLBACKIMG } from "../constants/fallbackimage";
import { fileAPI } from "../api/FileAPI";
import { formatPrice } from "../IntlFormat";

const { RangePicker } = DatePicker;

interface ProductOverViewItem extends IProduct {
  available: boolean;
}

const ProductOverviewList = forwardRef<RefCall, {}>((_props, ref) => {
  const { t } = useTranslation();

  const [products, setproducts] = useState<ProductOverViewItem[]>([]);
  const [loading, setloading] = useState(false);
  const [page, setpage] = useState<number>(1);

  const [filterparams, setfilterparams] = useState<ProductFilters>({
    searchterm: "",
    sort: "ASC",
    categories: "",
    startdate: "",
    enddate: "",
  });

  const [totallength, settotallength] = useState<number>(0);
  const defaultPageSize: number = 15;

  const navigate: NavigateFunction = useNavigate();

  const cartCtx = useContext(CartContext);

  useEffect(() => {
    setfilterparams((filters: any) => ({
      ...filters,
      startdate:
        cartCtx.timespan === null
          ? ""
          : cartCtx.timespan[0].format("YYYY-MM-DD"),
      enddate:
        cartCtx.timespan === null
          ? ""
          : cartCtx.timespan[1].format("YYYY-MM-DD"),
    }));
    handleFiltering(
      {
        ...filterparams,
        startdate:
          cartCtx.timespan === null
            ? ""
            : cartCtx.timespan[0].format("YYYY-MM-DD"),
        enddate:
          cartCtx.timespan === null
            ? ""
            : cartCtx.timespan[1].format("YYYY-MM-DD"),
      },
      1,
      defaultPageSize
    );
  }, []);

  useImperativeHandle(ref, () => ({
    callByRef(value: string) {
      handleCategoryFilter(value);
      handleFiltering({ ...filterparams, categories: value });
    },
  }));

  const handleRangeFilter = (dates: [Dayjs, Dayjs] | null) => {
    if (dates) {
      const startdate = dates[0].format("YYYY-MM-DD");
      const enddate = dates[1].format("YYYY-MM-DD");
      setfilterparams((filters: any) => ({
        ...filters,
        startdate: startdate,
        enddate: enddate,
      }));
      handleFiltering({
        ...filterparams,
        startdate: startdate,
        enddate: enddate,
      });
      cartCtx.setTimeSpan(dates);
    } else {
      setfilterparams((filters: any) => ({
        ...filters,
        startdate: "",
        enddate: "",
      }));
      cartCtx.setTimeSpan(dates);
      handleFiltering({ ...filterparams, startdate: "", enddate: "" });
    }
  };

  const handleSort = (e: string) => {
    const sortvalues = e.split(";");
    setfilterparams((filters: ProductFilters) => ({
      ...filters,
      sort: sortvalues[1] as "ASC" | "DESC",
      sortBy: sortvalues[0] as "Renting Fee" | "Name",
    }));
    handleFiltering({
      ...filterparams,
      sort: sortvalues[1] as "ASC" | "DESC",
      sortBy: sortvalues[0] as "Renting Fee" | "Name",
    });
  };

  const handleCategoryFilter = (categoryValue: string) => {
    setfilterparams((filters: any) => ({
      ...filters,
      categories: categoryValue,
    }));
    handleFiltering({ ...filterparams, categories: categoryValue });
  };

  const handleSearchFilter = (searchterm: string) => {
    setfilterparams((filters: any) => ({ ...filters, searchterm: searchterm }));
    handleFiltering({ ...filterparams, searchterm: searchterm });
  };

  const handleFiltering = (
    filters: ProductFilters,
    page = 1,
    take = defaultPageSize
  ) => {
    const fetchfiltered = async () => {
      try {
        setloading(true);
        const result = await productAPI.fetchProducts(page, take, filters);
        const products = result.data as PagedAvailabilityList<IProduct[]>;
        setproducts([
          ...products.listAvailable.map((item: IProduct) => ({
            ...item,
            available: true,
          })),
          ...products.listUnavailable.map((item: IProduct) => ({
            ...item,
            available: false,
          })),
        ]);

        setloading(false);
        settotallength(products.totalAvailable + products.totalUnavailable);
      } catch (error) {
        setloading(false);
        console.error(error || t("product_overview_list.get_products_error"));
      }
    };
    fetchfiltered();
  };

  const handleaddtocart = (product: IProduct) => {
    const cartProduct = { ...product, amount: 1 };
    cartCtx.addProduct(cartProduct);
  };

  const handleNavigation = (id: bigint) => {
    navigate(`produkt/${id}`, { state: filterparams.timespan });
  };

  const imageurls = products.map((product) =>
    product.ProductImages!.length > 0
      ? fileAPI.getProductImage(
          product.ProductId!,
          product.ProductImages![0].ImageId
        )
      : undefined
  );

  return (
    <>
      <div className="flex flex-row flex-wrap lg:flex-nowrap gap-4">
        <Search
          className="flex-1 basis-1/2"
          placeholder={t("general.search")}
          allowClear
          size="large"
          value={filterparams.searchterm}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
            handleSearchFilter(event.target.value)
          }
        />
        <Select
          className="w-full basis-1/2 text-start flex-1"
          size="large"
          placeholder={t("general.sort_by")}
          onChange={(e: any) => handleSort(e)}
          options={[
            { value: "Name;ASC", label: "Name (A-Z)" },
            { value: "Name;DESC", label: "Name (Z-A)" },
            { value: "Renting Fee;DESC", label: "Ausleihpreis (Absteigend)" },
            { value: "Renting Fee;ASC", label: "Ausleihpreis (Aufsteigend)" },
          ]}
        />
        <RangePicker
          size="large"
          className="flex-1 basis-1/2"
          placeholder={[
            t("product_overview_list.rent_from_placeholder"),
            t("product_overview_list.rent_until_placeholder"),
          ]}
          defaultValue={cartCtx.timespan ?? undefined}
          minDate={dayjs()}
          format={"DD.MM.YYYY"}
          onChange={(event: any) => handleRangeFilter(event)}
        ></RangePicker>
      </div>
      <Divider></Divider>
      <div className="w-full overflow-auto">
        {!loading ? (
          <List
            itemLayout="vertical"
            dataSource={products}
            header={
              <div className="hidden lg:grid grid-cols-5 bg-neutral-50">
                <div></div>
                <h3>{t("general.product")}</h3>
                <h3>{t("general.accessibility")}</h3>
                <h3>{t("general.price")}</h3>
                <h3>{t("general.actions")}</h3>
                <Divider className="lg:col-span-5 m-0"></Divider>
              </div>
            }
            renderItem={(item, index) => (
              <List.Item key={index}>
                <div
                  onClick={() => handleNavigation(item.ProductId!)}
                  className="grid grid-cols-1 lg:grid-cols-5 content-center items-center hover:bg-neutral-50 px-3 text-center lg:text-start"
                >
                  <div
                    className="text-center"
                    onClick={(e) => e.stopPropagation()}
                  >
                    <Image
                      className="object-scale-down"
                      src={imageurls[index]}
                      fallback={FALLBACKIMG}
                      height={200}
                    ></Image>
                  </div>
                  <div className="px-3 mt-2 lg:mt-0">
                    <h5 className="m-0 truncate">{item.Manufacturer}</h5>
                    <h3 className="hover:cursor-pointer my-1 truncate">
                      {item.Name}
                    </h3>
                  </div>
                  {item.available ? (
                    <h4 className="text-green-400 font-bold my-0">
                      {cartCtx.timespan
                        ? t("general.accessible")
                        : t("product_overview_list.no_timespan")}
                    </h4>
                  ) : (
                    <h4 className="my-0 text-red-400">
                      {t("product_overview_list.unavailable")}
                    </h4>
                  )}
                  <p className="font-bold">
                    {formatPrice(item.RentingFee)}
                    {t("product_overview_list.product_renting_fee_placeholder")}
                  </p>
                  <div
                    className="flex justify-evenly flex-wrap gap-5"
                    onClick={(e) => e.stopPropagation()}
                  >
                    <Button
                      className="flex-1"
                      type="primary"
                      disabled={!item.available || !cartCtx.timespan}
                      shape="round"
                      onClick={() => handleaddtocart(item)}
                    >
                      <ShoppingCartOutlined />
                      <div className="inline-block lg:hidden xl:inline-block ml-1">
                        {t("product_overview_list.go_to_shopping_cart")}
                      </div>
                    </Button>
                  </div>
                </div>
              </List.Item>
            )}
          ></List>
        ) : (
          <>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-5 content-center items-center hover:bg-neutral-50 px-3">
              <div className="text-center md:col-span-2 lg:col-span-1">
                <Skeleton.Image active></Skeleton.Image>
              </div>
              <div className="md:col-span-4">
                <Skeleton active></Skeleton>
              </div>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-5 content-center items-center hover:bg-neutral-50 px-3">
              <div className="text-center md:col-span-2 lg:col-span-1">
                <Skeleton.Image active></Skeleton.Image>
              </div>
              <div className="md:col-span-4">
                <Skeleton active></Skeleton>
              </div>
            </div>
          </>
        )}

        {!loading && products.length > 0 && (
          <div className="flex justify-center mt-8">
            <Pagination
              onChange={(page, pageSize) => {
                setpage(page);
                handleFiltering(filterparams, page, pageSize);
              }}
              total={totallength}
              current={page}
              pageSize={defaultPageSize}
            />
          </div>
        )}
      </div>
    </>
  );
});

export default ProductOverviewList;
