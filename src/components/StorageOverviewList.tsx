import { Button, Popconfirm, Table } from "antd";
import { IWarehouse } from "../../shared/interfaces/IWarehouse";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { useTranslation } from "react-i18next";

const StorageOverviewList = ({
  warehouselist = [],
  handleDelete,
  handleEdit,
}: {
  warehouselist: IWarehouse[] | undefined;
  handleDelete: Function;
  handleEdit: (selecteditem: bigint) => void;
}) => {
  const { t } = useTranslation();

  return (
    <Table
      pagination={false}
      dataSource={warehouselist.map((warehouse, index) => ({
        key: index,
        WarehouseId: warehouse.WarehouseId,
        Name: warehouse.Name,
        Adress: {
          Street: warehouse.Street,
          Postal: warehouse.Postal,
          City: warehouse.City,
          Country: warehouse.Country,
          Housenumber: warehouse.Housenumber,
        },
        Regions: warehouse.Regions,
      }))}
      scroll={{ x: 500 }}
      columns={[
        {
          title: t("storage_overview_list.warehouse_name"),
          dataIndex: "Name",
          key: "warehousename",
        },
        {
          title: t("storage_overview_list.adress"),
          dataIndex: "Adress",
          key: "street",
          render: (adress) => (
            <div>{`${adress?.Street} ${adress?.Housenumber}, ${adress?.Postal} ${adress?.City}`}</div>
          ),
        },
        {
          title: t("general.actions"),
          width: "20%",
          key: "actions_warehouse",
          render: (warehouse: IWarehouse) => (
            <>
              <div className="flex flex-wrap md:flex-nowrap gap-5 items-center justify-center">
                <Button
                  onClick={() => handleEdit(warehouse.WarehouseId as bigint)}
                  className="flex-1 text-blue-500"
                  icon={<EditOutlined />}
                >
                  {t("storage_overview_list.edit_warehouse")}
                </Button>
                <Popconfirm
                  title={t("storage_overview.delete_warehouse_confirmation")}
                  onConfirm={() => handleDelete(warehouse.WarehouseId)}
                >
                  <Button
                    danger
                    type="link"
                    className="flex-1"
                    icon={<DeleteOutlined />}
                  >
                    {t("storage_overview_list.delete_warehouse")}
                  </Button>
                </Popconfirm>
              </div>
            </>
          ),
        },
      ]}
    ></Table>
  );
};

export default StorageOverviewList;
