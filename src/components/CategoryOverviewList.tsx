import {
  Button,
  Empty,
  Modal,
  Popconfirm,
  Skeleton,
  Table,
  message,
} from "antd";
import { useEffect, useState } from "react";
import { DeleteOutlined, EditOutlined, PlusOutlined } from "@ant-design/icons";
import { ICategory } from "../../shared/interfaces/ICategory";
import Search from "antd/es/input/Search";
import CategoryModal from "./modal/CategoryModal";
import CategoryItemList from "./CategoryItemList";
import { useTranslation } from "react-i18next";
import { categoryAPI } from "../api/CategoryAPI";
import axios from "axios";

const CategoryOverviewList = ({
  modalopen,
  setmodalopen,
}: {
  modalopen: { showCategoryModal: boolean; showMaterialModal: boolean };
  setmodalopen: Function;
}) => {
  const { t } = useTranslation();

  const [messageAPI, contextHolder] = message.useMessage();

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [showcategorylist, setCategoryList] = useState(false);
  const [searchterm, setSearchTerm]: [string, Function] = useState("");
  const [categorydata, setCategoryData]: [ICategory[], Function] = useState([]);
  const [selectedCategory, setSelectedCategory]: [
    ICategory | undefined,
    Function
  ] = useState();

  const transformdata = (categories: ICategory[]) => {
    const treedata = categories.map(
      (
        category
      ): {
        Name: string;
        ParentCategoryId: any;
        CategoryId: bigint | undefined;
        children: any;
        key: bigint | undefined;
      } => ({
        Name: category.Name,
        ParentCategoryId: category.ParentCategoryId,
        CategoryId: category.CategoryId,
        children: [],
        key: category.CategoryId,
      })
    );
    treedata.forEach((node) =>
      node.ParentCategoryId === null
        ? (node.children = categories.filter(
            (item) => node.key === item.ParentCategoryId?.CategoryId
          ))
        : []
    );
    return treedata
      .map((item) => ({
        ...item,
        children:
          item.children.length > 0
            ? item.children.map((subcategory: ICategory) => ({
                ...subcategory,
                key: subcategory.CategoryId,
              }))
            : undefined,
      }))
      .filter((node) => node.ParentCategoryId === null);
  };

  const getCategories = async () => {
    try {
      setLoading(true);
      const categories = await categoryAPI.fetchCategories();
      setCategoryData(categories.data.list);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      setError(true);
      console.error(error || t("general.get_categories_error"));
    }
  };

  useEffect(() => {
    getCategories();
  }, []);

  const handleCategorySearch = (searchvalue: string) => {
    setSearchTerm(searchvalue);
  };

  const handleCategoryEdit = (category: ICategory | undefined) => {
    if (category) {
      setmodalopen({ showCategoryModal: true, showMaterialModal: false });
      setSelectedCategory(category);
    } else {
      if (axios.isAxiosError(error)) {
        messageAPI.error(error.message);
        console.error(error.message);
      } else {
        console.error(t("category_overview_list.get_item_error"));
      }
    }
  };

  const handleCategorySubmit = async (categoryname: string, parent: number) => {
    try {
      await categoryAPI.createCategory(categoryname, parent);
      await getCategories();
      setmodalopen(false, false);
      setSelectedCategory(null);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        messageAPI.error(error.message);
        console.error(error.message);
      } else {
        console.error(error);
      }
    }
  };

  const handleDelete = async (categoryId: number) => {
    try {
      await categoryAPI.deleteCategory(BigInt(categoryId));
      await getCategories();
    } catch (error) {
      if (axios.isAxiosError(error)) {
        messageAPI.error(error.message);
        console.error(error.message);
      } else {
        console.error(error);
      }
    }
  };

  const handleCategoryUpdate = async (categoryname: string, parent: bigint) => {
    if (selectedCategory?.CategoryId) {
      try {
        await categoryAPI.updateCategory(
          selectedCategory.CategoryId,
          categoryname,
          parent
        );
        await getCategories();
        setSelectedCategory(null);
      } catch (error) {
        if (axios.isAxiosError(error)) {
          messageAPI.error(error.message);
          console.error(error.message);
        } else {
          console.error(error);
        }
      }
    }
  };

  const closeModal = () => {
    setmodalopen(false, false);
    setSelectedCategory(null);
  };

  const handleCategoryListModal = (category: ICategory) => {
    setSelectedCategory(category);
    setCategoryList(true);
  };

  return (
    <>
      {contextHolder}
      <div className="flex flex-row flex-wrap gap-4 mb-4 lg:w-1/2">
        <Search
          className="flex-1"
          placeholder="Suche"
          allowClear
          size="large"
          value={searchterm}
          onChange={(event) => handleCategorySearch(event.target.value)}
        />
      </div>
      {!loading ? (
        <>
          <Table
            columns={[
              {
                title: t("general.category"),
                key: "category",
                render: (item) => (
                  <div
                    className="cursor-pointer"
                    onClick={() => handleCategoryListModal(item)}
                  >
                    {item.Name}{" "}
                    <span className="font-italic">
                      {item.children?.length > 0 &&
                        `[${item.children?.length}]`}
                    </span>
                  </div>
                ),
              },
              {
                title: t("general.actions"),
                key: "actions",
                width: "30%",
                render: (category) => (
                  <div className="flex gap-5 flex-wrap">
                    <Button
                      className="flex-1 text-blue-500"
                      icon={<EditOutlined />}
                      onClick={() =>
                        handleCategoryEdit(
                          categorydata.find(
                            (item) => item.CategoryId === category.CategoryId
                          )
                        )
                      }
                    >
                      {t("general.edit")}
                    </Button>
                    <Popconfirm
                      title={t(
                        "category_overview_list.delete_category_confirmation"
                      )}
                      onConfirm={() => handleDelete(category.CategoryId)}
                    >
                      <Button
                        danger
                        type="link"
                        className="flex-1"
                        icon={<DeleteOutlined />}
                      >
                        {t("general.delete")}
                      </Button>
                    </Popconfirm>
                  </div>
                ),
              },
            ]}
            dataSource={transformdata(categorydata).filter((item) =>
              item.Name.toLowerCase().includes(searchterm.toLowerCase())
            )}
          ></Table>
          <Modal
            centered
            open={showcategorylist}
            onCancel={() => {
              setCategoryList(false);
              setSelectedCategory();
            }}
          >
            <CategoryItemList category={selectedCategory}></CategoryItemList>
          </Modal>
          <Modal
            destroyOnClose
            centered
            open={modalopen.showCategoryModal}
            cancelText={t("general.cancel")}
            onCancel={closeModal}
            okButtonProps={{
              type: "primary",
              form: "categoryForm",
              htmlType: "submit",
              icon: <PlusOutlined />,
            }}
          >
            <CategoryModal
              handlesubmit={
                selectedCategory ? handleCategoryUpdate : handleCategorySubmit
              }
              selectedCategory={selectedCategory}
            ></CategoryModal>
          </Modal>
        </>
      ) : !error ? (
        <Skeleton></Skeleton>
      ) : (
        <Empty></Empty>
      )}
    </>
  );
};

export default CategoryOverviewList;
