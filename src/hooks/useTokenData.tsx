import { useEffect, useState } from "react";
import { getAuthToken } from "../util/auth";

const useTokenData = () => {
  const [token, setToken] = useState<string | undefined>(getAuthToken());

  useEffect(() => {
    const handleChangeStorage = () => {
      setToken(getAuthToken());
    };
    window.addEventListener("storage", handleChangeStorage);
    return () => window.removeEventListener("storage", handleChangeStorage);
  });

  return [token, setToken] as const;
};

export default useTokenData;
