import { IOrder } from "../../shared/interfaces/IOrder";

export interface IOrderProps {
  orders: IOrder[];
}
