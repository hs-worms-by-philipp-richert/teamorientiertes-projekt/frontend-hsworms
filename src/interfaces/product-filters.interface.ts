import {Dayjs} from 'dayjs'

export interface ProductFilters {
    searchterm: string,
    sort: 'ASC' | 'DESC' | undefined,
    sortBy?: 'Renting Fee' | 'Name'
    categories?: string,
    timespan?: [Dayjs, Dayjs],
    startdate?: string,
    enddate?: string,
}