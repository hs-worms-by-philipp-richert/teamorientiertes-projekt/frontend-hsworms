import { IUser } from "../../shared/interfaces/IUser";

export interface IUserProps {
  userId: string;
  dataForm:IUser|undefined,
  setFunction:React.Dispatch<React.SetStateAction<IUser | undefined>>
}
