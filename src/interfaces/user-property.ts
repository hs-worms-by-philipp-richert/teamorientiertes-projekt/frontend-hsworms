export interface User {
  Firstname: string;
  Lastname: string;
  UserId: string;
  Street: string;
  Postal: string;
  City: string;
  Housenumber: string;
  Country: string;
  Email: string;
  Phone: string;
  HasAcknowledgedMaterialOrder: boolean;
  Password: string;
  RoleId: number;
}
